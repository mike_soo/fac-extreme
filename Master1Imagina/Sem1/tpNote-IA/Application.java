package tp1BackTrack;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Application {
	public static void main(String[] args) throws Exception{
		String fileNameExt ="constReseau.txt"; 
		String fileNameEg ="constReseauEg.txt"; 
		String fileNameDif ="constReseauDif.txt";
		String fileNameExp ="constReseauExp.txt";
		String instance ="instance.txt";
		Network myNetwork;
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+fileNameDif);
		BufferedReader readFile = new BufferedReader(new FileReader (fileNameDif));
		myNetwork = new Network(readFile);
		readFile.close();
//		Network(BufferedReader); 
//		System.out.println("\nMon réseau de contraintes (les entrees incorrectes ayant ete ignorees) :\n" + myNetwork);
//		System.out.println("Test function getConstraints(WA)");
//		System.out.println(myNetwork.getConstraints("WA"));
		
//		for(Constraint c : myNetwork.getConstraints("WA"))
//		{
////			System.out.println(c.toString());
////			System.out.println(c.getArity());
//			System.out.println("name");
//			System.out.println(c.name);
//			System.out.println("varList");
//			System.out.println(c.varList);
//			System.out.println("getTuples()");
//			System.out.println(c.getTuples());
//			
//			
//		}
//		System.out.println("Construction example");
		CSP csp = new CSP(myNetwork);
//		ArrayList <Assignment> alAsignment = csp.searchAllSolutions();
//		System.out.println(alAsignment);
		csp.maxCardEtDegree();
//		System.out.println("EOL");
		csp.searchAllSolutionsBT();
//		 csp.searchAllSolutionsFC();
//		csp.searchSolutionsFC();

	}
}
