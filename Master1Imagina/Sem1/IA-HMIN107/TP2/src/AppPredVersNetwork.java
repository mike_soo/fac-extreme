
 
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import structure.Atom;
import java.util.ArrayList;

import structure.KnowledgeBase;
import tp1CSP.CSP;
import tp1CSP.Network;

public class AppPredVersNetwork {
	public static void main(String[] args) throws Exception {
		/**
		 * EXEMPLE d'application de la solution proposée 
		 * pour l'enonce de l'étape 3 : 3.
		 * Il s'agit de l'aplication du backtrack grace à la transformation
		 * de cette regle en une instance de csp.
		 * On passe donc d'une regle et d'une base de fait 
		 * vers un CSP, on trouve toutes les solutions du CSP grace au 
		 * BT puis on convertit les assignations générés en atomes
		 * d'ordres 0 qu'on rajoute à notre basse de fait saturé (partiel)
		 * 
		 * Cette exemple marche pour un fichier d'entré contenant
		 * une seul regle (composé de n predicats) et d'une base de fait.
		 * 
		 */
		String tp2test ="tp2RegleVersCSP.txt";
		
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+tp2test);
		BufferedReader readFile = new BufferedReader(new FileReader (tp2test));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		readFile.close();
		
		
		
		kb.generateAllSubstitutions();
		kb.FCcompteurs();
		
		Network net = new Network();
//		
		Atom reqp0 = new Atom( "p(x,y)");
		Atom reqp1 = new Atom ("p(y,z)");
		ArrayList <Atom> requete = new ArrayList <Atom>();
		requete.add(reqp0);
		requete.add(reqp1);
		
		net.setNetworkFromRequete(requete, kb.getSaturedFactBase(), kb.getAllTermsFromKB("cst"));

		
		CSP csp = new CSP(net);
		System.out.println("\n\nCSP from KB : " + csp);
		csp.maxCardEtDegree();
//		
//		System.out.println("\nSEARCHSOLUTIONBT");
//		csp.searchSolutionBT();

		System.out.println("\nSEARCHALLSOLUTIONBT");
		csp.searchAllSolutionsBT();
		
		
		kb.getHomsFromCSPAssignmentSolutions(csp.getSolutions(), requete);
			
	}
}
