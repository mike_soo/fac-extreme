package mains_TP2_ordre_0_et_1;

 
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import structure.Atom;
import java.util.ArrayList;

import structure.KnowledgeBase;
import tp1CSP.CSP;
import tp1CSP.Network;

public class Exemple_de_requete_ordre1 {
	public static void main(String[] args) throws Exception {
		/**
		 * Exemple (extrait du cours) d'application de la solution proposée 
		 * pour l'enonce de l'étape 3 : 3.
		 * Il s'agit de l'application du backtrack grace à la transformation
		 * de requete en instance de csp.
		 * On passe donc d'une requete et d'une base de fait 
		 * vers un CSP, on trouve toutes les solutions du CSP grace au 
		 * BT puis on convertit les assignations générés en atomes
		 * d'ordre 0 qu'on renvois comme resultat de la requete.
		 
		 */
		String tp2test ="tp2RegleVersCSP.txt";
		
		System.out.println(" chargement du fichier :"+ new java.io.File(".").getCanonicalPath()+"/"+tp2test);
		BufferedReader readFile = new BufferedReader(new FileReader (tp2test));

		KnowledgeBase kb = new KnowledgeBase(readFile);
		readFile.close();
		
		
		
		kb.generateAllSubstitutions();
		kb.FCcompteurs();
		kb.showSaturedFactBase();
		Network net = new Network();
//		Tout les chemins de x à y
		Atom reqp0 = new Atom( "chemin('e','f')");
		
		ArrayList <Atom> requete = new ArrayList <Atom>();
		requete.add(reqp0);
		
		
		System.out.println("\n\nCSP from requete :\n ");
		net.setNetworkFromRequete(requete, kb.getSaturedFactBase(), kb.getAllTermsFromKB("cst"));

		System.out.println("creation de CSP");
		CSP csp = new CSP(net);
		
		csp.maxCardEtDegree();
		
//		
//		System.out.println("\nSEARCHSOLUTIONBT");
//		csp.searchSolutionBT();

		System.out.println("\nSEARCHALLSOLUTIONBT");
		csp.searchAllSolutionsBT();
		
		
		kb.getHomsFromCSPAssignmentSolutions(csp.getSolutions(), requete);
			
	}
}
