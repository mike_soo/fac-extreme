package tp1BackTrack;
import java.util.ArrayList;
import java.util.HashMap;


public class InterArrayList
{
	public static ArrayList <Object> intersection(ArrayList <Object> d1 , ArrayList <Object> d2)
	{
		HashMap <String , Object> dverif = new HashMap();

			// dfinal = d1 inter d2
			ArrayList <Object> dfinal = new ArrayList<Object>();


			for(Object o1 : d1)
			{
				dverif.put(o1.toString(),o1);
			}

			for(Object o2 : d2)
			{
				if(dverif.get(o2.toString()) != null)
				{
					dfinal.add(o2);
				}
			}
			// System.out.println(varModif + "=" + varsDom.get(varModif));
			
			return dfinal;
	}
}