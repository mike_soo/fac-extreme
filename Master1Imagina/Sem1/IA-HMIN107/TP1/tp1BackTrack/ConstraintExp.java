package tp1BackTrack;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;


public class ConstraintExp extends Constraint
{	
	String expression;
	public ConstraintExp(BufferedReader in ) throws Exception
	{
		super(in);
		expression=in.readLine();
	}

	public boolean violation(Assignment a) 
	{
		boolean accepted = true;
		if (a.getVars().containsAll(varList))
		{
		try {
			
				ScriptEngineManager mgr = new ScriptEngineManager();
				ScriptEngine engine = mgr.getEngineByName("JavaScript");
				for (String var : varList){
					engine.put(var, a.get(var));
					
				}
				accepted = (boolean) engine.eval(expression); // résultat de l'évaluation
			}
		catch (ScriptException e) { System. err .println("probleme dans: "+ expression); }
		}
		return !accepted;
	}

	@Override
	public ArrayList<ArrayList<Object>> getTuples() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected HashMap<String, ArrayList<Object>> modify(String var, HashMap<String, ArrayList<Object>> varDom, Assignment assignement) {
		return new HashMap<String, ArrayList<Object>>();
		// TODO Auto-generated method stub
		
	}
}