package tp1CSP;

import java.util.ArrayList;

public class InfoVar
{
	protected int voisinSelected;
	protected ArrayList <Constraint> inContraints;
	protected int indInOrderedVars;
	InfoVar()
	{
		voisinSelected=0;
		inContraints=new ArrayList <Constraint>();
	}

	protected ArrayList <Constraint> getContraints()
	{
		return inContraints;
	}

	InfoVar(ArrayList<Constraint> inContraints)
	{
		voisinSelected=0;
		this.inContraints = inContraints;
	}

	protected void setIndInOrderedVars(int ind)
	{
		this.indInOrderedVars = ind;
	}

	protected int getIndInOrderedVars()
	{
		return this.indInOrderedVars;
	}

	protected void addContraint(Constraint c)
	{
		inContraints.add(c);
	}

	protected void voisinSelected()
	{
		voisinSelected++;
	}

	protected int getNbVoisinSelected()
	{
		return voisinSelected;
	}
}