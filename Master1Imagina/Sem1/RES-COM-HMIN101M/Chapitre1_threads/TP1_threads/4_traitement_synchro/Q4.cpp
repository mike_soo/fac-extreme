#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

#define PX 7000
#define NBTREADS 5

struct OrganiserTraitement
{
	pthread_mutex_t *verrou;
	pthread_cond_t *cond;
	unsigned int *image;
	unsigned int id;
	unsigned int *d;
};

void *Ti(void *params)
{
	OrganiserTraitement* ot = (OrganiserTraitement*) params;
	// sleep(0.5);
	unsigned int id=ot->id;
	bool jaiVerrou=true;
	
	pthread_mutex_lock(ot->verrou);
	// cout<<"id="<<id<<" &id="<<ot->id<<endl;
	cout<<"th-id:"<<id<<" d[id]="<<ot->d[id]<<" start"<<endl;

	cout<<"th-id:"<<id<<" d[id]="<<ot->d[id]<<" active"<<endl;		
	while( ot->d[id]<PX )
	{
		
		if(id == 0)
		{
			// cout<<"th-id "<<id<<" d[id]="<<ot->d[id]<<"++"<<endl;
			ot->d[id]+=1;	
			// pthread_mutex_unlock(ot->verrou);
			pthread_cond_broadcast((ot->cond));	
			cout<<"th-id "<<id<<" d[id]="<<ot->d[id]<<" j'attends1"<<endl;
			pthread_cond_wait(ot->cond,ot->verrou);
			// jaiVerrou=false;

		}	
		//Si mon id de thread est différent de 0 est que le thread prédécesseur 
	// 	// traite une zone plus en avant que le thread courant
		else if( ot->d[id-1]>ot->d[id]) 
		{
			// cout<<"th-id "<<id<<" d[id]="<<ot->d[id]<<"++"<<endl;
			ot->d[id]+=1;
			// pthread_mutex_unlock(ot->verrou);			
			pthread_cond_broadcast((ot->cond));
			cout<<"th-id "<<id<<" d[id]="<<ot->d[id]<<" j'attends2"<<endl;
			if(id!=NBTREADS - 1)
				pthread_cond_wait(ot->cond,ot->verrou);
			// jaiVerrou=false;
		}
		
		else
		{
			
			pthread_cond_broadcast((ot->cond));
			cout<<"th-id "<<id<<" d[id]="<<ot->d[id]<<" j'attends3"<<endl;
			pthread_cond_wait((ot->cond),(ot->verrou));		
			// cout<<"th-id "<<id<<" d[id]="<<ot->d[id]<<" je reveil"<<endl;
			// jaiVerrou=true;
		}

		if( ot->d[id]<PX)
		{
			for(int i=0;i<NBTREADS;i++)
			{
				cout<<"id->"<<i<<" ";
				// for(int j=0;j<ot->d[i];j++)
				// {
				// 	cout<<".";
				// }
				cout<<ot->d[i];
				cout<<endl;			
			}				
			cout<<endl;
		}
		else
		{

			pthread_mutex_unlock(ot->verrou);
			break;
		}
		
		
	}
	cout<<"th-id:"<<id<<" d[id]="<<ot->d[id]<<" done"<<endl;		
	// chaque arrivant réveil quelqu'un 
	
	
}

int main()
{	
	pthread_t idT[NBTREADS];

	OrganiserTraitement ot[NBTREADS];
	unsigned int ids[NBTREADS];
	unsigned int image[PX];
	unsigned int d[NBTREADS];
	

	pthread_mutex_t verrou = ((pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER );
	pthread_cond_t cond = ((pthread_cond_t)PTHREAD_COND_INITIALIZER);
	
	
	for (unsigned int i = 0 ; i < NBTREADS; i++)
	{
		d[i]=0;
		ids[i]=i;

	}
	
	for (int i = 0 ; i < PX; i++)
	{
		image[i]=0;
	}
	


	for(unsigned int i=0;i<NBTREADS;i++)
	{
		ot[i].verrou= &verrou;
		ot[i].cond  = &cond;
		ot[i].image = image;
		ot[i].d     = d;
		ot[i].id    = ids[i];
		pthread_create (&idT[i],NULL,&Ti,(void*) &ot[i]);
	}
	

	// cout<<"threads crées"<<endl;
	
	for (int i = 0 ; i<NBTREADS ; i++)
	{
		pthread_join(idT[i], NULL);
	}
	

  
  return 0;
}
