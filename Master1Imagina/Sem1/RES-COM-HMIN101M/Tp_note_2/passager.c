#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>

#define NB_SEMS 2
#define BUS_DEP 0
#define BUS_ARR 1
#define NB_CASE_MEMSP 0

int gestionErr(int e,char const *f);
int gestionErr(void* e,char const *f);
int gestionErr(int e,char const *f,int id);
void sig_handler(int signo);
// Affiche l'état d'un tableau de sémaphore active 
// et d'une zone de mémore partagé en fonction des parametres.
void afficheSemsMemP( int idSem, int nb_sems , char * ptrMemP ,\
						unsigned int nb_case_memsP, unsigned int ptr_type_size);
union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};


int passager(int idSem , int idPas)
{
	struct sembuf attent_dep={(u_short)(BUS_DEP),(short)-1,0};
	struct sembuf retour_voy={(u_short)(BUS_ARR),(short)-1,0};

	printf("idPas=%i j'attends bus\n",idPas);
	

	int res = semop(idSem , &attent_dep, 1);
	gestionErr(res, "attente bus ",idPas); 
	

	printf("idPas=%i je monte dans le bus\n",idPas);
	fflush(stdout);
	
	
	res = semop(idSem , &retour_voy , 1);
	gestionErr(res, "retour _voy ",idPas);

	printf("idPas=%i je descend du bus !\n",idPas);	
	fflush(stdout);
	return 0;
}