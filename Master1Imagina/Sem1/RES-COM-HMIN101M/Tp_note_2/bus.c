#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>

#define NB_SEMS 2
#define BUS_DEP 0
#define BUS_ARR 1
#define NB_CASE_MEMSP 0

int gestionErr(int e,char const *f);
int gestionErr(void* e,char const *f);
int gestionErr(int e,char const *f,int id);
void sig_handler(int signo);

// Affiche l'état d'un tableau de sémaphore active 
// et d'une zone de mémore partagé en fonction des parametres.
void afficheSemsMemP( int idSem, int nb_sems , char * ptrMemP ,\
						unsigned int nb_case_memsP, unsigned int ptr_type_size);
union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};



int bus(int idSem , int cap_max_passagers , int duree_voyage)
{

	struct sembuf attent_dep={(u_short)(BUS_DEP),(short)0,SEM_UNDO};
	struct sembuf retour_voy={(u_short)(BUS_ARR),(short)cap_max_passagers,SEM_UNDO};
	struct sembuf attend_decs={(u_short)(BUS_ARR),(short)0,SEM_UNDO};
	sleep(2);

	while(1)
	{
		printf("bus attend pour partir\n ");
		
		int res = semop ( idSem, &attent_dep, 1);
		gestionErr(res, " semop attend partir");
		
		
		printf("\nbus voyage\n\n");
		sleep(duree_voyage);


		printf("bus retour du voyage\n");
		printf ("bus attend que tout le monde descend\n");
		
		//crée sem at BUS_ARR representer la descente
		retour_voy.sem_num = BUS_ARR;
		res = semop (idSem , &retour_voy,1);
		gestionErr(res, " semop retour_voy");


		//attend sem at BUS_ARR = 0 représente l'attente de la descente
		res = semop (idSem , &attend_decs,1);
		gestionErr(res, "semop attentd desc");

		// La vitesse pour que tous les sémaphores tombe à 0
		// (indice BUS_ARR) est nettement supérieur 
		// à la vitesse d'affichage de tous les processus 
		// indiquant qu'ils sont bien descendus du bus
		// Le sleep suivant ne représente en AUCUN CAS
		// l'attente de la descente des processus. 
		// Cette attente est réussi grâce au semop ci dessus.
		sleep(1);

		retour_voy.sem_num= BUS_DEP;
		res = semop(idSem , &retour_voy , 1);
		

	}	
	return 0;
}