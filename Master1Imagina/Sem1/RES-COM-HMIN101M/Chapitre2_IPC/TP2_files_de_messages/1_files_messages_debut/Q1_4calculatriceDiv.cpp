#include "Q1_2structs.cpp"

using namespace std;

int main()
{
	key_t sesame =ftok("./cle.txt",1);
	if(sesame < 0) 
	{
		cout<<"ftok():"<<strerror(errno)<<endl;
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0664);
	if(f_id < 0) 
	{
		cout<<"msgget():"<<strerror(errno)<<endl ;
		return -1;
	}

	struct reqsansop requetesansop;
	struct rep reponse;
	// char buf[33];
	printf("j’attends un message\n");
	while(1)
	{

		// On lit que les messages possédant une certaine étiquette, 
		// puis on en extrait de celui ci, l'étiquette correspondant
		// à l’émetteur pour ainsi lui renvoyer une réponse.
		msgrcv(f_id,(void *) &requetesansop,sizeof(struct reqsansop),44,0);
		reponse.mtype = requetesansop.mtypeClient;
		
		printf("%i / %i\n",requetesansop.a,requetesansop.b);
		
		reponse.r = requetesansop.a / requetesansop.b;
		msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
		
		
			
	}

	if (msgctl(f_id, IPC_RMID,NULL) <0)
	{
		cout<<"msgctl() : "<<strerror(errno)<<endl;
	}


	return 0;
}
