#include "Q1_2structs.cpp"

using namespace std;

int main()
{
	key_t sesame =ftok("./cle.txt",1);
	if(sesame < 0) 
	{
		cout<<"ftok():"<<strerror(errno)<<endl;
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0664);
	if(f_id < 0) 
	{
		cout<<"msgget():"<<strerror(errno)<<endl ;
		return -1;
	}

	struct req requete;
	struct rep reponse;
	// char buf[33];
	while(1)
	{
		// On lit tout les messages rentrant, comme il s’agit de 
		// plusieurs messages reçu par plusieurs client, 
		// on les reçoit tous, puis on les traites en fonction
		// de chaque étiquette extraite de chaque message.
		msgrcv(f_id,(void *) &requete,sizeof(struct req),0,0);
		reponse.mtype = requete.mtype + 1 ;

		switch(requete.op)
		{
			case '*' :
				printf("%i * %i",requete.a,requete.b);
				reponse.r = requete.a * requete.b;
				// snprintf(buf, sizeof(int), "%d", requete.a);
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;
			
			case '+':
				printf("%i + %i",requete.a,requete.b);
				reponse.r = requete.a + requete.b;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;
			
			case '-':
				printf("%i - %i",requete.a,requete.b);
				reponse.r = requete.a - requete.b;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;

			case '/':
				printf("%i - %i",requete.a,requete.b);
				reponse.r = requete.a / requete.b;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;
			default: 
				reponse.r = -1;
				msgsnd(f_id,(void *) &reponse,sizeof(struct rep),0);
				break;

		}
		printf("message repondu avec etiq= %li\n",reponse.mtype);
	}

	if (msgctl(f_id, IPC_RMID,NULL) <0)
	{
		cout<<"msgctl() : "<<strerror(errno)<<endl;
	}


	return 0;
}
