
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <math.h>

struct test{
	long mtype;
	char T[4];
};


int getFileMessCle(char * chemin, int canal)
{

	key_t sesame =ftok(chemin,canal);
	if(sesame < 0) 
	{
		printf("ftok():%s\n",strerror(errno));
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0664);
	if(f_id < 0) 
	{
		printf("msgsnd():%s\n",strerror(errno));
		return -1;
	}

	return f_id;
}

void fils(int msgid)
{
	struct test t;
	t.mtype=1;

	t.T[0]='h';
	t.T[1]='e';
	t.T[2]='y';
	t.T[3]='\n';

	int res=msgsnd(msgid,(void *) &t,4*sizeof(char),0);
	
	res=msgrcv(msgid,(void *) &t,4*sizeof(char),2,0);	
	printf("fils: j'ai recu %s\n",t.T);
}


void pere(int msgid)
{
	struct test t;

	int res=msgrcv(msgid,(void *) &t,4*sizeof(char),1,0);
	printf("pere: j'ai recu %s\n",t.T);

	t.mtype=2;
	t.T[0]='h';
	t.T[1]='o';
	t.T[2]='y';
	t.T[3]='\n';
	res=msgsnd(msgid,(void *) &t,4*sizeof(char),0);

}


int main(int argc,char * argv[])
{
	int msgid=getFileMessCle("./cle.txt",2);

	pid_t pid = fork();

	if(pid ==0 )
	{
		fils(msgid);
		return 1;
	}
	else
	{
		pere(msgid);
	}

	return 0;
}