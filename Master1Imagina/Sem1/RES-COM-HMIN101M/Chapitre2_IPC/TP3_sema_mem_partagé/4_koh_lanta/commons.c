#include "commons.h"

bool sig_intACT;


void sig_handler(int signo)
{
  if (signo == SIGINT)
    {
    	printf("received SIGINT\n");
		sig_intACT = true;
	}
}


int gestionErr(int e , const char* f)
{
	if (e < 0)
	{
		printf("Erreur > %s : %s\n",f, strerror(errno));
		exit(-1);
	}
	else 
		return 1;

}

int gestionErr(void *e, const char* f)
{
	if((void*) e == (void*) -1)
	{
	
		printf("Erreur > %s : %s\n",f , strerror(errno));
		exit(-1);

	}
	else 
		return 1;
}

int gestionErr(int e, const char* f,int id)
{
	if (e < 0)
	{
		printf("id = %i Erreur > %s : %s\n",id,f , strerror(errno));
		exit(-1);
	}
	else 
		return 1;
}


int getFileMessCle(key_t sesame,char * chemin, int canal)
{

	
	if(sesame < 0) 
	{
		printf("ftok():%s\n",strerror(errno));
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0600	);
	if(f_id < 0) 
	{
		printf("msgsnd():%s\n",strerror(errno));
		return -1;
	}

	return f_id;
}


void afficheSemsMemP( int idSem, int nb_sems , char * ptrMemP ,\
						unsigned int nb_case_memsP, unsigned int ptr_type_size)
{

	unsigned short  sems_vals[nb_sems];
	semun s;
	char * p = ptrMemP;
	s.array = sems_vals;
	int res = semctl(idSem, 0 /*semno ignored*/ ,GETALL ,  s.array);
	gestionErr(res , "afficheSemsMemP");
	printf("\n");
	for(int i = 0 ; i < nb_sems; i++)
	{
		printf("%-3i",i);
	}
	printf("\n\e[1;31m");
	
	for(int i = 0 ; i < nb_sems; i++)
	{
		printf("%-3i",s.array[i]);			
	}
	printf("\e[0m\n");
	
	// Affichage mémoire partagé (valeur de zone en cour de traitement)
	
	for(unsigned int i = 0 ; i < nb_case_memsP; i++)
	{
		printf("%-3i",i);
	}

	printf("\n\e[1;32m");
	unsigned int i = 0 ;
	for(p = ptrMemP ; i < nb_case_memsP ; p = p + ptr_type_size)
	{
		printf("%-3u",*p);			
		i++;
	}
	printf("\e[0m\n");
}