#include "commons.h"
#include "commonsTP.h"

extern bool sig_intACT;


int main(int argc , char ** argv)
{
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	
	if (signal(SIGINT, sig_handler) == SIG_ERR) {
        fputs("Erreur lors de signal handler.\n", stderr);
        return EXIT_FAILURE;
    }
	int res;

		
	// init valeur de semaphores à 0
	unsigned short sems_vals [NB_SEMS];

	for(int i = 0 ; i < NB_SEMS ; i ++  )
	{
		sems_vals[i]=0;
	}

	int idSem = semget(sesame, NB_SEMS ,IPC_CREAT | 0600);
	gestionErr(idSem,"semget");
	

	semun s;
	s.array = sems_vals;


	res = semctl( idSem , 0 , SETALL , s);
	gestionErr(res,"semctl");

	//init valeurs de mémoire partagé.
	int idMem = shmget(sesame, NB_EQS*sizeof(unsigned int), IPC_CREAT|0600);
	gestionErr(idMem,"shmget idMemEq1");
	
	unsigned int *ptrMemP = (unsigned int*)shmat(idMem,NULL,0);
	
	

	while(1)
	{
		afficheSemsMemP(idSem, NB_SEMS,(char *)ptrMemP,NB_EQS ,sizeof(unsigned int));
		sleep(1);
	

		
		if(sig_intACT)
		{
			break;
		}	
	}	

	res=shmctl(idMem,IPC_RMID,0);
	gestionErr(res,"shmctl");	
	res=semctl(idSem,IPC_RMID,0);
	gestionErr(res,"semctl");	
	
	return 0;
}