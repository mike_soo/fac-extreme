#include "commons.h"
#include "commonsTP.h"

void traitement_joueur(int njo , int neq,int idSem , unsigned int* ptrMemP)
{
	srand(time(NULL) ^ (getpid()<<16));

    struct sembuf opp={(u_short)(njo),(short)1,0};
	// semaphore -1
	struct sembuf opv={(u_short)njo,(short)-1,0};
	int res;
    
    // Réunion tous les équipes
    opp.sem_num = IND_REUNION; 	
	res = semop ( idSem , &opp, 1 );
	gestionErr(res, "semop reunion tous les équipes");
	
	
	printf("idj=%i , neq=%i : attends tour pour courir \n", njo , neq);
	gestionErr(res , "semop attente de tour pour courir");
	
	// J'attends mon tour pour courir 
	opv.sem_num = njo;
	res = semop( idSem , &opv , 1);
	
	
	int r = (rand() % 4) + 1;
	printf("idj=%i , neq=%i : je cours pendant %is \n", njo , neq,r);

	sleep(r); 

	// Je suis revenue 
	int liquide_ramne=(rand() % 10) + 1;
	ptrMemP[neq]+=liquide_ramne;
	printf("idj=%i , neq=%i : je ramene %i goutte \n", njo , neq,liquide_ramne);

	opp.sem_num = IND_REUNION;
	res = semop(idSem , &opp , 1);
	gestionErr(res , "semop retour");
	
	printf("idj=%i , neq=%i revenue\n" , njo , neq );

	for (int i =0 ; i < NB_EQS ; i++ )
	{
		printf ("%u " , ptrMemP[i] );

	}

	afficheSemsMemP (idSem , NB_SEMS, (char *)ptrMemP, NB_EQS, sizeof(unsigned int) );

	printf("\n");

}

void traitement_equipes(int neq , int idSem , unsigned int* ptrMemP)
{
	
	int pid;
	
	for (int i = 0 ; i < NB_J_PAR_EQ ; i++)
	{
	    pid = fork();
	    if (pid == 0 )
	    {
	    	sleep(1);
	        traitement_joueur(i ,neq, idSem , ptrMemP);
	        return ;
	    }
	}
	
	
	wait();

	
}


int main(int argc , char ** argv)
{
	int res;
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	gestionErr(sesame,"ftok");
	
	int idSem = semget(sesame, NB_SEMS , 0600);
	gestionErr(idSem,"semget");
			
	unsigned short sems_vals[NB_SEMS];
	for(int i = 0 ; i < NB_SEMS  ; i ++ )
	{
		sems_vals[i] = 0;
	}
	
	semun s;
	s.array = sems_vals;

	// init semaphores
	res = semctl( idSem , 0 , SETALL , s);
	gestionErr(res,"semctl");

	// get mémoire partagé
	int idMem = shmget(sesame, NB_EQS*sizeof(unsigned int), 0600);
	gestionErr(idMem,"shmget idMemEq1");
	
	// get ptr vers mémoire partagé
	unsigned int *ptrMemP = (unsigned int*)shmat(idMem,NULL,0);

	// reinitialisation de la mémoire partagé.
	for(int i =0 ; i < NB_EQS ; i++)
	{
		ptrMemP[i]=0;
	}

	// Création des deux équipes
	pid_t pid;
	for(int i =0 ; i< NB_EQS ; i++)
	{	
		pid = fork();	
		if (pid == 0)
		{
			traitement_equipes(i , idSem, ptrMemP);
			return 0;
		}


	}
	
	
	

	// J'attends pour que les joueurs de tous
	// les equipes soit prets. 
	printf ("attente pour que tous les joueurs soient prets\n");
	struct sembuf opwait = {(u_short)IND_REUNION,(short) -(NB_EQS * NB_J_PAR_EQ) ,SEM_UNDO};
	
	res = semop (idSem , &opwait , 1 );
	gestionErr(res , "semop : attente que tous les joueurs soient prêt");

	printf("prets ! \n");
	printf("La cours commence\n");
	
	for(int i = 0 ; i < NB_J_PAR_EQ ; i++)
	{
		
		// Donne NB_EQS sémaphores à l'indice i du tab de sémaphores 
		// où NB_EQS joueurs attendent après demande d'un sémaphore 
		// par joueur. Ceci permet la sortie synchronisé des joueurs 
		// vers leurs bassine respectives.  
		opwait.sem_num = (u_short)i;
		opwait.sem_op = NB_EQS;
		res = semop(idSem , &opwait, 1 ); 
		gestionErr(res , "semop sortie synchronisé");
		printf("sortie synchronisé\n");
		// Demande de NB_EQS sémaphores à l'indice IND_REUNION 
		// ce qui représente l'attente du retour des coureurs 
		// provenant de leurs bassines respective. 
		opwait.sem_num = (u_short) IND_REUNION;
		opwait.sem_op = -NB_EQS;
		res = semop(idSem , &opwait , 1);
		gestionErr(res ," attente du retour des coureurs");
		


		// Affichage des sémaphores 
		
	}



	return 0;
}
