#include "commons.h"


extern bool sig_intACT;

int main(int argc , char ** argv)
{
	char chemin[20] = "./cle.txt";
	key_t sesame =ftok(chemin,1);
	
	if (signal(SIGINT, sig_handler) == SIG_ERR) {
        fputs("Erreur lors de signal handler.\n", stderr);
        return EXIT_FAILURE;
    }
	int res;

	//Creation filedemessage
	int f_id = getFileMessCle(sesame,chemin, 1);
	gestionErr(f_id, "getFileMessCle");

	//Creation shared memory
	int idMem = shmget(sesame, sizeof(unsigned int), IPC_CREAT | 0600);
	gestionErr(idMem, "shmget" );
	
	//Creation semaphore
	int idSem = semget(sesame, 1 , IPC_CREAT | 0600);
	gestionErr(idSem ,"semget");
	
	union semun sem_config;
	sem_config.val=1;
	semctl(idSem,0,SETVAL,sem_config);


	// get pointeur vers mémoire partagé
	int *ptrMemP = (int*)shmat(idMem,NULL,0);
	gestionErr((void *) ptrMemP,"shmat");

	// création d’opération des sémaphores
	// sémaphore +1
	struct sembuf opp={(u_short)0,(short)1,SEM_UNDO};
	// sémaphore -1
	struct sembuf opv={(u_short)0,(short)-1,SEM_UNDO};
	
	// Initialisation de mémoire partagé à 20 
	// <=> il y a 20 places disponible au début.

	res=semop(idSem,&opv,1);
	*ptrMemP = 20;
	res=semop(idSem,&opp,1);


	while(1){
		sleep(1);
		printf("Working\n");
		if(sig_intACT)
		{
			break;
		}	
	}	

	res=shmctl(idMem,IPC_RMID,0);
	gestionErr(res,"shmctl");	
	res=semctl(idSem,IPC_RMID,0);
	gestionErr(res,"semctl");	
	res=msgctl(f_id,IPC_RMID,0);
	gestionErr(res,"msgctl");	

	return 0;
}