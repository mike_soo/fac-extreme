#include "commons.h"

bool sig_intACT;


void sig_handler(int signo)
{
  if (signo == SIGINT)
    {
    	printf("received SIGINT\n");
		sig_intACT = true;
	}
}


int gestionErr(int e , const char* f)
{
	if (e < 0)
	{
		printf("Erreur > %s : %s\n",f, strerror(errno));
		exit(-1);
	}
	else 
		return 1;

}

int gestionErr(void *e, const char* f)
{
	if((void*) e == (void*) -1)
	{
	
		printf("Erreur > %s : %s\n",f , strerror(errno));
		exit(-1);

	}
	else 
		return 1;
}

int gestionErr(int e, const char* f,int id)
{
	if (e < 0)
	{
		printf("id = %i Erreur > %s : %s\n",id,f , strerror(errno));
		exit(-1);
	}
	else 
		return 1;
}


int getFileMessCle(key_t sesame,char * chemin, int canal)
{

	
	if(sesame < 0) 
	{
		printf("ftok():%s\n",strerror(errno));
		return -1;
	}
	
	int f_id =msgget(sesame, IPC_CREAT | 0600	);
	if(f_id < 0) 
	{
		printf("msgsnd():%s\n",strerror(errno));
		return -1;
	}

	return f_id;
}

