package tp6DesignPatterns;

import java.util.ArrayList;

import java.util.HashMap;

public abstract class AbDirectory extends Structure{	
	abstract public ArrayList<Structure> getContent();
	abstract public int nbElements();
	abstract public void addStructure(Structure s);
	abstract public void rmStructure(Structure s);
	
}