package tp6DesignPatterns;

public abstract class Structure  {
	
	private String name;
	private Structure structureParent;
	
	abstract public int basicSize();
	abstract public String ls();
	
	abstract public int size();
	
	public String getName()
	{
		return name;
	}
	public String getAbsoluteAddress() {
		
		// TODO Auto-generated method stub
		if(structureParent == null)
		{
			return "/" + this.name;
		}

		else
		{
			return structureParent.getAbsoluteAddress() 
					+ this.name; 
		}
	}

	


	public Structure getParent() {
		// TODO Auto-generated method stub
		
		return structureParent;
	}
	
}