//package mochito;
//
//import static org.mockito.Mockito.*;
//import org.junit.*;
//import org.junit.runner.RunWith;
//import org.mockito.junit.MockitoJUnitRunner;
//
//
//
//@SuppressWarnings("unused")
//@RunWith(MockitoJUnitRunner.class)
//
//public class TestMock {
//	
//	private I mock=mock(I.class);;
//	
//	
//	@Test
//	public void test1() throws Exception
//	{
//		Assert.assertEquals(0,mock.methodeInt());
//			
//	}
//	
//	@Test
//	public void test2() throws Exception
//	{
//		when(mock.methodeInt()).thenReturn(1,2,3,4);
//		Assert.assertEquals(1, mock.methodeInt());
//		Assert.assertEquals(2, mock.methodeInt());
//		Assert.assertEquals(3, mock.methodeInt());
//		Assert.assertEquals(4, mock.methodeInt());
//		
//		verify(mock,times(4)).methodeInt();
//	}
//	@Test
//	public void test3() throws Exception 
//	{
//		doThrow(new Exception()).when(mock).methodeVoid();
//		try
//		{
//			mock.methodeVoid();
//		}
//		catch (Exception e)
//		{	
//			System.out.println("Exception lors de l'appel à "
//					+ "mock.methodeVoid()");
//		}
//	}
//	@Test
//	public void test4()
//	{
//		
//		when(mock.methodeParam(0)).thenReturn(0);
//		when(mock.methodeParam(5)).thenReturn(10);
//		when(mock.methodeParam(3)).thenReturn(3);
//		Assert.assertEquals(0, mock.methodeParam(0));
//		Assert.assertEquals(3, mock.methodeParam(3));
//		Assert.assertEquals(10, mock.methodeParam(5));
//	}
//}
//	