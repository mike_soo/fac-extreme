package test_Surcharge_Redef;

import java.util.Vector;

public class Test {
	public static void main(String[] args) 
	{
//		Vector<X> xs  = new Vector <Z>(); // ne compile pas
		Y y = new Y();
		Z z = new Z();
		A a = new B();
		B b = new B();
//		a.f(y);
		b.f(z);
		a.f(z);
	}
}

class X{}
class Y{}
class Z extends X{} 

class A
{
	public void f(X x){System.out.println("A.f(X x)");}
}

class B extends A
{
	public void f(Y y) {System.out.println("B.f(Y y)");}
	
	public void f(Z z) {System.out.println("B.f(Z z)");}
}
