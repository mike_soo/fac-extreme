package tp7AbstractFactory;

abstract class AGenerator
{
	public abstract void generate(String a);
}