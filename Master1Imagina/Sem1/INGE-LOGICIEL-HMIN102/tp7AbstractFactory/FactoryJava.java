
package tp7AbstractFactory;

public class  FactoryJava extends AFactoryCompiler {

	public LexerJava createLexer()
	{
		return new LexerJava();
	}
	public ParserJava createParser()
	{
		return new ParserJava();
	}
	public GenJava createGenerator()
	{
		return new GenJava();
	}
	
	
}