package tp8PatronDecorator;

public class Compte extends ACompte{
	

	public Compte(AClient client)
	{
		super(client);
		
	}
	public Compte(AClient client, ACompte decoratorCompte)
	{
		super (client ,decoratorCompte);
	}
	
	public float getPrixLocation(AProduit p)
	{
		return (float ) p.getPrix();
	}
	
	public float getPrixLocationDecorator(AProduit p)
	{
		if(decoratorCompte == null)
		{
			return p.getPrix();
		}
		else
		{
			return decoratorCompte.getPrixLocationDecorator(p);
			
					
		}
	}
}