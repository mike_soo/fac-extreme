package tp8PatronDecorator;

public abstract class AProduit {
	
	protected String nom ;
	protected float prix;
	public AProduit(String nom , float prix)
	{
		this.nom = nom;
		this.prix = prix;
	}
	
	public abstract float getPrix();
	
}