package tp9PatronCommande;

public class Vider extends Command {
	

	public Vider(Bidon source) {
		super(source);
		// TODO Auto-generated constructor stub
	}

	public void doit()
	{
		setQteEauUtilise( getSource().getVolumeCourant());
		getSource().setVolumeCourant(0);
	}
	
	public void undo()
	{
		getSource().setVolumeCourant( getQteEauUtilise());
	} 

}
