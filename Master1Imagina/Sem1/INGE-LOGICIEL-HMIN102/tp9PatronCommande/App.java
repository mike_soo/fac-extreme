package tp9PatronCommande;


public class App {
	
	public static void main(String[] args) throws Exception 
	{
		Jeu j = new Jeu ( 5 );
		
		j.commandDo("r", 2 , 0);
		j.afficheBidons();
		j.commandDo("r", 3 , 0);
		j.afficheBidons();
		j.commandDo("r", 4 , 0);
		j.afficheBidons();
		j.commandDo("t", 3, 4);
		j.afficheBidons();
		
		j.commandUndo();
		j.afficheBidons();
		j.commandUndo();
		j.afficheBidons();
	} 
}
