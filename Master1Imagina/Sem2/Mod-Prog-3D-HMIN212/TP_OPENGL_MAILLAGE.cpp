//INSERT CE QUE J'AI RECOPIER SUR FEUILLE
// page 1 et 2 05/03/2018

int j = 0 

for (int i =0 ; i < nbTriangles ; i++)
{
	file>>nbInd;
	if nbInd == 3
	{
		file>>indices[3*j];
		file>>indices[3*j + 1];
		file>> "" ""  3*j +2;
		j++;
	}
}



// VBO 
//Espace mémoire dans la carte graphique permettant de stocker les données
GLuint vbo[2];
glGenBuffers(2,vbo); //definitoin
glBindBuffer(GL_ARRAY_BUFFER, &vbo[0]); //creation
glBufferData(GL_ARRAY_BUFFER , sizeof(float) 3 * nbPoints , coordinates , GL_STATIC_DRAW); //remplisage


//VAO 
glGenVertexArrays(1,&vao);
glBindVertexArray(vao);
glBindBuffer(GL_ARRAY_BUFFER , vbo);

//On indique où est ce que ce trouve les coord de nos sommets
glVertexPointer( 3 , //dimension nombre d'infos representant une coordonnées
				GL_FLOAT , //type
				0, //com entre vbos
				0 //pour associer au cpu , on prefere utiliser vbo d'où le 0
				)
//activation du vbo
glEnableClientState(GL_VERTEX_ARRAY);
					GL_ELEMENT_ARRAY_BUFFER; //element ebo pour stocker les indices de chaque sommet

glNormalPointer() //pour donner les normes de chaque points



//Get time of chaque frame
current = glutGet(GLUT_ELLAPSED_TIME);
float deltaTime=(current - previous) *1000 //transformation en seconde;
float radius = 10.0f;
float corx = sin(current) * radius + target.x ;
float cory = cos(current) * radius + target.y ;




gluLookAt(camera[0], camera[1], camera[2], /* look from camera XYZ */ 
			0, 0, 0, /* look at the origin */ 
			0, 1, 0); /* positive Y up vector */ 
			glRotatef(orbitDegrees, 0.f, 1.f, 0.f);/* orbit the Y axis */
			 /* ...where orbitDegrees is derived from mouse motion */ glCallList(SCENE); /* draw the scene */