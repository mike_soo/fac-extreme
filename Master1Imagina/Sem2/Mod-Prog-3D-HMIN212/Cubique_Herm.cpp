#include "Cubique_Herm.hpp"

Cubique_Herm::Cubique_Herm(Vector * cubiqueHermPoints, Vector * points, size_t nbU)
{
	this->cubiqueHermPoints = cubiqueHermPoints;
	this->points= points;
	this->nbU= nbU - 1;

}

double Cubique_Herm::f1(double u)
{
	return (double) (2 *  pow(u,3)) - (3 * pow(u,2)) + 1;  
}

double Cubique_Herm::f2(double u)
{
	return (double) (-2 * pow(u,3)) + (3 * pow(u,2));  	
}

double Cubique_Herm::f3(double u)
{
	return (double) (     pow(u,3)) - (2 * pow(u,2)) + u;  	
}

double Cubique_Herm::f4(double u)
{
	return (double) (	 pow(u,3)) -       pow(u,2) ;  	
}


void Cubique_Herm::hermiteCubicCurve(Draw * d)
{

	for (unsigned int i = 0 ; i <= nbU; i++)
	{
		points[i]= (f1((double)  i/nbU ) * cubiqueHermPoints[0])  
				  +(f2((double)  i/nbU ) * cubiqueHermPoints[1] )  
				  +(f3((double)  i/nbU ) * (cubiqueHermPoints[2] - cubiqueHermPoints[0]) )  
				  +(f4((double)  i/nbU ) * (cubiqueHermPoints[1] - cubiqueHermPoints[3])); 
	}
	
	d->setPoints(this->points,this->nbU + 1);
	d->drawCurve();

}