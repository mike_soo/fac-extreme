#ifndef BEZIER_SURFACE_HPP
#define BEZIER_SURFACE_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "BezierCurveByBernstein.hpp"
#include <stdio.h>
#include <stdlib.h>

extern Base base0;
extern Base base1;
class BezierSurface {

private:
	Vector ***tabControlPoints;
	BezierCurveByBernstein *bcurve;
	long nbU,nbV;
	int nbControlPoints_i , nbControlPoints_j, prof_k , prof_attente;
	Vector *base[3];
public:
	BezierSurface(int nbControPoints_i,int nbControlPoints_j);
	BezierSurface(int nbControlPoints_i , int nbControlPoints_j , Vector *vi , Vector *vj , Vector * vk);
	BezierSurface(int nbControlPoints_i , int nbControlPoints_j , BezierCurveByBernstein *bcurve);
	void init(int nbControlPoints_i , int nbControlPoints_j);
	void setControlPoint(int i , int j , Vector& p_contr );
	void calcSurfacePointCasteljau(double u , double v);
	Vector* getPointSurfaceCasteljau();
	Vector* getControlPoint(int i , int j);
	void drawSurfacePointCasteljau(Draw *d);
	void drawSurfaceControlPoints(Draw *d);
	void drawSurfaceCasteljau(unsigned int nbU , unsigned int nbV , Draw *d);
	
};
#endif