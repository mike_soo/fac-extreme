

#include "Point.hpp"

Point::Point() : _x(0), _y(0) , _z(0)
{}

Point::Point(double x , double y ,double z): _x(x), _y(y) , _z(z)
{}


double Point::getX()
{
	return _x;
}


double Point::getY()
{
	return _y;
}

double Point::getZ()
{
	return _z;
}
