#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "Figure2D.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <vector>
#include <set>

class Triangle : public	 Figure2D 
{
private:
	Vector * points[3];
	Vector * norme=NULL;
public :
	Triangle(Vector *p1 , Vector *p2 , Vector *p3);
	virtual void drawNorme(Draw *d);
	virtual void calcNorme();
	virtual Vector * getNorme();
	virtual vector <Figure2D*> * getVoisins();
	virtual Vector * getPoint(int i);
	virtual void drawTrLignes( Draw *d );
	virtual void drawTrLignes( Draw *d , float r , float g , float b );
	virtual bool contientPoint(Vector * p);
	// Obtient le point non inclue dans this dans deux triangles voisins données
	virtual Vector *getSommetDeTrVoisinNonCommun(Triangle * triangleVois);
	static Figure2D *getCommonTriangle(vector <Vector*> * points);
	virtual unsigned int getNombrePoints();
};

#endif

