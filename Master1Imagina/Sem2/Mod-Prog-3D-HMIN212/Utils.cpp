#include "Utils.hpp"
extern float ortho;

void Utils::readOFF(Base *base , vector <Figure2D*>* figures2D , string nomFichierOFF)
{
	string line;
	ifstream source;
	source.open(nomFichierOFF, ios_base::in);
	if(!source)
	{
		cerr<<"Imposible d'ouvrir le fichier"<<endl;
		return;
	}

	getline(source,line);
	std::istringstream in(line);      //make a stream for the line itself

    std::string type;
    in >> type; // Lecture de OFF
    cout<<"Type : "<<type<<endl;
    if(type != "OFF")       //and check its value
    {
    	cerr<<"type de fichier invalide"<<endl;
    	return;
    }
	
    unsigned long long int nb_points, nb_aretes , nb_non_util;
    getline(source,line);
	in.str(line);
	in.seekg(0);
	cout <<"in "<<in.str()<<endl;

    in >> nb_points >> nb_aretes >> nb_non_util;
	cout <<"nb_points_aretes : "<<nb_points <<" "<< nb_aretes <<" "<< nb_non_util<<endl;
	double x, y, z;
    vector <Vector*>* points = new vector <Vector*>();
    
    unsigned long long int idMaillage = Maillage::getIdMaillageDispo();
    cout<<"idMaillage pour figure.off : "<< idMaillage<<endl;;
    for (unsigned long long int i = 0 ; i < nb_points; i ++)
    {
        getline(source,line);
        in.str(line);      //make a stream for the line itself
        in.seekg(0);

        in >> x >> y >> z;       //now read the whitespace-separated floats
        cout <<"P"<<i<<"="<<x<<" "<<y<<" "<<z<<endl;
        points->push_back(new Vector(x , y , z , *base));
        points->back()->idsMaillages->insert(idMaillage);

            
    }
    if(points->size()< 2)
    {
        cerr<<"Utils::readOFF : Erreur, pas assez de points pour trouver max et min"<<endl;
        return;
    }
    
    unsigned int nb_points_figure;
    unsigned long long int index_fig0 , index_fig1 , index_fig2;
    for (unsigned long long int i = 0 ; i < nb_aretes; i ++)
    {
    	getline(source,line);
    	in.str(line);
    	in.seekg(0);

    	in>>nb_points_figure;
    	
    	if( nb_points_figure == 3)
    	{
    		in >> index_fig0 >> index_fig1 >> index_fig2;
    		cout<<"indexes " << index_fig0 <<" "<< index_fig1 <<" " <<index_fig2<<endl; 

    		Triangle * t = new Triangle(points->at(index_fig0) , points->at(index_fig1) , points->at(index_fig2));
    		figures2D->push_back(t);

    		points->at(index_fig0)->addFigure2D(figures2D->back()); 
    		points->at(index_fig1)->addFigure2D(figures2D->back()); 
    		points->at(index_fig2)->addFigure2D(figures2D->back());
    	}

    }


    delete points;

    
}


// void load_obj(const char* filename, vector<glm::vec4> &vertices, vector<glm::vec3> &normals, vector<GLushort> &elements)
// {
//     ifstream in(filename, ios::in);
//     if (!in)
//     {
//         cerr << "Cannot open " << filename << endl; exit(1);
//     }

//     string line;
//     while (getline(in, line))
//     {
//         if (line.substr(0,2) == "v ")
//         {
//             istringstream s(line.substr(2));
//             glm::vec4 v; s >> v.x; s >> v.y; s >> v.z; v.w = 1.0f;
//             vertices.push_back(v);
//         }
//         else if (line.substr(0,2) == "f ")
//         {
//             istringstream s(line.substr(2));
//             GLushort a,b,c;
//             s >> a; s >> b; s >> c;
//             a--; b--; c--;
//            elements.push_back(a); elements.push_back(b); elements.push_back(c);
//         }
//         else if (line[0] == '#')
//         {
//             /* ignoring this line */
//         }
//         else
//         {
//             /* ignoring this line */
//         }
//     }

//     normals.resize(vertices.size(), glm::vec3(0.0, 0.0, 0.0));
//     for (int i = 0; i < elements.size(); i+=3)
//     {
//         GLushort ia = elements[i];
//         GLushort ib = elements[i+1];
//         GLushort ic = elements[i+2];
//         glm::vec3 normal = glm::normalize(glm::cross(
//         glm::vec3(vertices[ib]) - glm::vec3(vertices[ia]),
//         glm::vec3(vertices[ic]) - glm::vec3(vertices[ia])));
//         normals[ia] = normals[ib] = normals[ic] = normal;
//     }
// }