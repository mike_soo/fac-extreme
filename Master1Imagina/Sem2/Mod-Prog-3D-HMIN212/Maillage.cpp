#include "Maillage.hpp"
extern int ind_voisins;
extern float ortho;

unsigned long long int Maillage::idMaillageDispo =0;

unsigned long long int Maillage::getIdMaillageDispo() 
{
	return Maillage::idMaillageDispo++;
}

void Maillage::afficheMaillageTrs(Draw *d , vector <Figure2D*>* figures2D)
{
	unsigned long long int nb_points = figures2D->size()*9;
	unsigned long long int nb_arete = figures2D->size()*3;
	GLfloat * data_vertex = new GLfloat[nb_points];
	GLuint * data_index = new GLuint[nb_arete];
	unsigned long long int i_vertex=0; 
	for(unsigned int i = 0 ; i < figures2D->size(); i++ )
	{
		for(unsigned int j = 0 ; j < 3 ; j++)
		{
			data_vertex[i_vertex      ] = (float)figures2D->at(i)->getPoint(j)->getX();

			data_vertex[i_vertex + 1  ] = (float)figures2D->at(i)->getPoint(j)->getY();
			
			data_vertex[i_vertex + 2  ] = (float)figures2D->at(i)->getPoint(j)->getZ();				
			
			
			i_vertex +=3;	
			
		}

		figures2D->at(i)->drawTrLignes(d , 1 , 0 , 0 );
		
		// figures2D->at(i)->drawNorme(d);


	}
	
	

	unsigned long long int i_index=0;
	for(unsigned int i = 0; i < figures2D->size()*3 ; i +=3)
	{
		
		data_index[i_index]   = i;
		data_index[i_index+1] = i + 1 ;
		data_index[i_index+2] = i + 2;	
	
		i_index+=3;
	}

	// d->drawPoint(&p_sud);
	
	d->drawTrianglesVbo(data_vertex, data_index , nb_points * sizeof(GLfloat) , nb_arete * sizeof(GLuint));
	delete data_vertex;
	delete data_index;
}


void Maillage::afficheMaillage(Draw *d , vector <Figure2D*>* figures2D , float r , float g , float b)
{
	for(unsigned int i = 0 ; i < figures2D->size(); i++ )
	{
		figures2D->at(i)->drawTrLignes(d , r , g , b );
		// figures2D->at(i)->calcNorme();
		// figures2D->at(i)->drawNorme(d);
	}
}


void Maillage::spotAngleDiedre(float angleSeuil , Draw *d , vector <Figure2D*>* figures2D)
{
	
	for(unsigned int i = 0 ; i < figures2D->size() ; i++)
	{
		vector <Figure2D*>* voisins = figures2D->at(i)->getVoisins();
		if (voisins == NULL)
		{
			cerr<<"warning ! spotAngleDiedre: voisins == NULL"<<endl;
			return;
		}

		for(unsigned int j = 0 ; j < voisins->size() ; j ++)
		{
			float angleDiedreAvVoisin = figures2D->at(i)->calcAngleDiedre(voisins->at(j)) * 180/M_PI;
			cout<<"angle dièdre "<< angleDiedreAvVoisin<<endl;
			if(angleDiedreAvVoisin < angleSeuil )
			{
				
				figures2D->at(i)->drawTrLignes(d , 1.0 , 1.0 , 0.0 );
			}
			else
			{
				figures2D->at(i)->drawTrLignes(d);
			}
		}
	}	
	
	
}

 
void Maillage::butterflySubdivision(vector <Figure2D*> *maillage, vector <Figure2D*> *maillageSubdivisee , Base * base)
{ 
	Draw d;
	vector <Vector*>*Ei = new vector<Vector*>();
	unsigned long long int idMaillage = Maillage::getIdMaillageDispo();
	for(unsigned int i = 0 ; i < maillage->size(); i++)
	{
		// cout<<"i ="<<i << " maillage size = "<<maillage->size()<<endl;
		Vector *d1first=NULL , *d2first=NULL , *d3first=NULL;
		// cout<<"Maillage::butterflySubdivision() = "<<i<<endl;
		for(unsigned int j = 0 ; j < 3; j++)
		{	

			// cout<<"\t j = "<<j<<endl;
			// cin.get();
			// cout<<"Maillage::butterflySubdivision() : j="<<j<<endl;
			Vector *d1 = maillage->at(i)->getPoint((j  )%3);
			Vector *d2 = maillage->at(i)->getPoint((j+1)%3);
			Vector *d3 = maillage->at(i)->getPoint((j+2)%3);
			
			if(j == 0)
			{
				d1first = d1;
				d2first = d2;
				d3first = d3;
			}
			Vector *d4=NULL , *d5=NULL , *d6=NULL , *d7=NULL , *d8=NULL;
			
			// cout<<"Maillage::butterflySubdivision() :"<<"d1->getId() : "<<d1->getId() <<"  d2->getId() : "<< d2->getId()<<endl;
			map<unsigned long long int , Vector *>::iterator itpairsSub = d1->pairsDeSubdivision->find(d2->getId());

			
		
			
			
			set<Vector *>::iterator it;
			
			set<Vector*> *d6candidats = d1->getSommetsCommunsDansTrianglesAdjacents2D(d3);
			
			
			for(it = d6candidats->begin() ;it!= d6candidats->end();it++)
			{
				if(**it != *d2 )
				{
					d6=*it;
				}
			}
			delete d6candidats;


			set<Vector*> *d5candidats = d2->getSommetsCommunsDansTrianglesAdjacents2D(d3);
			for(it = d5candidats->begin() ;it!= d5candidats->end();it++)
			{
				if(**it != *d1 )
				{
					d5=*it;
				}
			}
			delete d5candidats;
			

			set<Vector*> *d4candidats = d1->getSommetsCommunsDansTrianglesAdjacents2D(d2);
			for(it = d4candidats->begin() ;it!= d4candidats->end();it++)
			{
				if(**it != *d3 )
				{
					d4=*it;
				}
			}
			delete d4candidats;
			
			
			

			if(d4 != NULL)
			{
				set<Vector*> *d7candidats = d2->getSommetsCommunsDansTrianglesAdjacents2D(d4);
				for(it = d7candidats->begin() ;it!= d7candidats->end();it++)
				{
					if(**it != *d1 )
					{
						d7=*it;
					}
				}
				delete d7candidats;
				
				set<Vector*> *d8candidats = d1->getSommetsCommunsDansTrianglesAdjacents2D(d4);
				for(it = d8candidats->begin() ;it!= d8candidats->end();it++)
				{
					if(**it != *d2 )
					{
						d8=*it;
					}
				}
				delete d8candidats;
				
			}
			
			

			

			if(ind_voisins == 0) 
			{
				d1->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d1->getId()<<endl;
			}
			else if(ind_voisins == 1) 
			{
				d2->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d2->getId()<<endl;
			}
			else if(ind_voisins == 2) 
			{
				d3->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d3->getId()<<endl;
			}
			else if(ind_voisins == 3) 
			{
				d4->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d4->getId()<<endl;
			}
			else if(ind_voisins == 4) 	
			{
				d5->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d5->getId()<<endl;
			}
			else if(ind_voisins == 5) 
			{
				d6->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d6->getId()<<endl;
			}
			else if(ind_voisins == 6) 
			{
				d7->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d7->getId()<<endl;
			}
			else if(ind_voisins == 7) 	
			{
				d8->affichePoint(&d , 0 , 1 , 1);
				// cout<<"Maillage::butterflySubdivision(): id="<<d8->getId()<<endl;
			}
			
			if(itpairsSub == d1->pairsDeSubdivision->end())	
			{
				Vector *E1 = new Vector(0,0,0,*base);
				float w=1.0/8.0;

				// 0.5 * (*d1 + *d2);
				
				*E1 = 0.5 * (*d1 + *d2) 
				+ w * (*d3 + (d4==NULL? Vector(0,0,0,*base)  : *d4)) 		
				-(w / 2.0) * ((d5==NULL? Vector(0,0,0,*base) : *d5) 
						+ (d6==NULL? Vector(0,0,0,*base) : *d6) 
						+ (d7==NULL? Vector(0,0,0,*base) : *d7) 
						+ (d8==NULL? Vector(0,0,0,*base) : *d8));
				
				// *E1 = 0.5 * (*d1 + *d2) + w * (*d3 + *d4) -(w / 2.0) * (*d5 + *d6 + *d7 + *d8);
				E1->affichePoint(&d , 1.0 , 1.0 , 1.0);	

				
				d1->pairsDeSubdivision->insert(pair<unsigned long long int , Vector*>(d2->getId() , E1)) ;
				d2->pairsDeSubdivision->insert(pair<unsigned long long int , Vector*>(d1->getId() , E1)) ;
				
				E1->parentsDeSubdivision->insert(pair<unsigned long long int , Vector*>(d1->getId() , d1)) ;
				E1->parentsDeSubdivision->insert(pair<unsigned long long int , Vector*>(d2->getId() , d2)) ;
				
				E1->parentsDeSubdivisionOrd->push_back(d1);
				E1->parentsDeSubdivisionOrd->push_back(d2);

				Ei->push_back(E1);
				
			}
			else
			{
				
				// cout<<"\td1->pairsDeSubdivision->find(d2->getId()) : "<<d1->getId()<<" " <<(d2->getId())<<endl;
				// Vector * pointSubDiv =d1->pairsDeSubdivision->at(d2->getId()); 
				// reverse((pointSubDiv->parentsDeSubdivisionOrd->begin()), (pointSubDiv->parentsDeSubdivisionOrd->end()));

				
				Ei->push_back(d1->pairsDeSubdivision->at(d2->getId()));
				
			}


			

			
		
		}
		
		for(unsigned int j = 0 ; j < Ei->size() ; j++)
		{
			Ei->at(j)->affichePoint(&d , 1 , 0.4 , 0.3);

		}
		
		d1first->idsMaillages->insert(idMaillage);
		d2first->idsMaillages->insert(idMaillage);
		d3first->idsMaillages->insert(idMaillage);

		Ei->at(0)->idsMaillages->insert(idMaillage);
		Ei->at(1)->idsMaillages->insert(idMaillage);
		Ei->at(2)->idsMaillages->insert(idMaillage);


		Triangle *t1 = new Triangle(d1first, Ei->at(2) , Ei->at(0));
			d1first->addFigure2D(t1);
			Ei->at(2)->addFigure2D(t1);
			Ei->at(0)->addFigure2D(t1);
			

		Triangle *t2 = new Triangle(d2first, Ei->at(0) , Ei->at(1));
			d2first->addFigure2D(t2);
			Ei->at(0)->addFigure2D(t2);
			Ei->at(1)->addFigure2D(t2);
		
		Triangle *t3 = new Triangle(d3first, Ei->at(1) , Ei->at(2));
				d3first->addFigure2D(t3);
				Ei->at(1)->addFigure2D(t3);
				Ei->at(2)->addFigure2D(t3);
		
		Triangle *t4 = new Triangle(Ei->at(0) , Ei->at(1) , Ei->at(2)  );
				Ei->at(0)->addFigure2D(t4);
				Ei->at(1)->addFigure2D(t4);
				Ei->at(2)->addFigure2D(t4);
		maillageSubdivisee->push_back(t1);
		maillageSubdivisee->push_back(t2);
		maillageSubdivisee->push_back(t3);
		maillageSubdivisee->push_back(t4);

		
		// for(int i = 0 ;i < 3 ; i ++)
		// {
		// 	Triangle *t = new Triangle(Ei->at(i) , Ei->at(i)->parentsDeSubdivisionOrd->at(0) , Ei->at(i)->parentsDeSubdivisionOrd->at(1)  );
		// 	Ei->at(i)->addFigure2D(t) ; 
		// 	Ei->at(i)->parentsDeSubdivisionOrd->at(0)->addFigure2D(t) ; 
		// 	Ei->at(i)->parentsDeSubdivisionOrd->at(1)->addFigure2D(t) ;
		// 	maillageSubdivisee->push_back(t);		
		// }
		// t1->drawTrLignes(&d , 0.2 , 0.0 , 0.6);
		// t2->drawTrLignes(&d , 0.2 , 0.0 , 0.6);
		// t3->drawTrLignes(&d , 0.2 , 0.0 , 0.1);
		// t4->drawTrLignes(&d , 0.2 , 0.0 , 0.6);


		Ei->clear();

		

	}


	
}

void Maillage::distances(vector <Figure2D*> *maillage1, vector <Figure2D*> *maillage2 , Draw *d , vector<Vector*>* points2a2plusProchesNaif)
{
	Vector *pointPlusProche=NULL , *pointEnCour=NULL;
	cout<<"nombre de points de maillage1 pour calcul distance naif : "<<maillage1->size()<<endl;
	for(unsigned int i = 0 ; i < maillage1->size() ; i++)
	{
		if(i % 500 == 0 )
			cout<<"nombre de points traitées = "<<i<<endl;
		for(unsigned int j = 0 ; j < 3; j++)
		{
			pointEnCour = maillage1->at(i)->getPoint(j);
			pointPlusProche = Maillage::calcPointPlusProche(pointEnCour , maillage2);

			if(pointEnCour== NULL || pointPlusProche == NULL)
				cerr<<"Maillage::distances() : Warning, pointEnCour== NULL || poinPlusProche == NULL"<<endl;
			else
			{
				points2a2plusProchesNaif->push_back(pointEnCour);
				points2a2plusProchesNaif->push_back(pointPlusProche);
				// d->drawLine(pointEnCour, pointPlusProche , 0.2 , 0.9 , 0.9);
			}
			
		}
	}

}


Vector * Maillage::calcPointPlusProche(Vector* p , vector <Figure2D*>* maillage)
{
	float distanceMin=numeric_limits<float>::max() , distanceTemp;
	Vector *pointPlusProche = NULL;
	for(unsigned int i = 0 ; i < maillage->size() ; i++)
	{
		for(unsigned int j = 0 ; j < 3 ; j++)
		{
			distanceTemp = maillage->at(i)->getPoint(j)->distance(p);
			if(distanceTemp < distanceMin)
			{
				distanceMin = distanceTemp;
				pointPlusProche = maillage->at(i)->getPoint(j);

			}

		}

	}	

	return pointPlusProche;

}



void Maillage::distancesVoxels(vector<Vector*>* pointsMaillages , Draw * d , unsigned long long int idMaillage1 , unsigned long long int idMaillage2 , vector<Vector*>* points2a2plusProches)
{
	VoxelG * voxelG ;
	Vector * pointPlusProcheCandidat , *pointCour , *pointPlusProche;
	float distanceMin;
	
	cout<<"Maillage::distancesVoxels() :"<< pointsMaillages->size();
	for(unsigned int i =  0 ; i < pointsMaillages->size(); i++)
	{	
		

		distanceMin=  std::numeric_limits<float>::max();
		pointCour = pointsMaillages->at(i);
		voxelG = pointCour->getVoxel();
		
		if(pointCour->idsMaillages->find(idMaillage2)!=pointCour->idsMaillages->end())		
			d->drawPoint(pointCour, 0 , 1  , 1 );
		if(pointCour->idsMaillages->find(idMaillage1)!= pointCour->idsMaillages->end())
			d->drawPoint(pointCour, 1 , 1 , 0);
		

		if(voxelG->getPointsContenus()->size() > 1)
		{
			pointPlusProcheCandidat=NULL ;
			pointPlusProche=NULL;
			
			for(unsigned int j = 0 ; j< voxelG->getPointsContenus()->size() ; j++)
			{
				pointPlusProcheCandidat=voxelG->getPointsContenus()->at(j);
				set<unsigned long long int>::iterator it1 = pointCour->idsMaillages->find(idMaillage1);
				set<unsigned long long int>::iterator it2 = pointPlusProcheCandidat->idsMaillages->find(idMaillage2);

				set<unsigned long long int>::iterator it3 = pointCour->idsMaillages->find(idMaillage2);
				set<unsigned long long int>::iterator it4 = pointPlusProcheCandidat->idsMaillages->find(idMaillage1);
				

				if(     
					 pointCour->distance(pointPlusProcheCandidat) < distanceMin
					 && (  
					 	  ( 
					 	  	it1 != pointCour->idsMaillages->end() 
					 	   && 
					 	    it2 != pointPlusProcheCandidat->idsMaillages->end()
					 	  )
					 	  ||
					 	  (
					 	  	it3 != pointCour->idsMaillages->end() 
					 	   && 
					 	    it4 != pointPlusProcheCandidat->idsMaillages->end()	

					 	  )
					    )
				  )
				{

					distanceMin = pointCour->distance(pointPlusProcheCandidat);
					pointPlusProche = pointPlusProcheCandidat;
				}
			}
			// cout<<"Maillage::voxelDistances() : drawLine  start"<<endl;
			if(pointPlusProche != NULL )
			{
				
				// cout<<"pointPlusProche.x="<<endl;
				// cout<<pointPlusProche->getX()<<endl;
				// cout<<"pointPlusProche.y="<<endl;
				// cout<<pointPlusProche->getY()<<endl;
				// cout<<"pointPlusProche.z="<<endl;
				// cout<<pointPlusProche->getZ()<<endl;

				// cout<<"pointCour.x="<<endl;
				// cout<<pointCour->getX()<<endl;
				// cout<<"pointCour.y="<<endl;
				// cout<<pointCour->getY()<<endl;
				// cout<<"pointCour.z="<<endl;
				// cout<<pointCour->getZ()<<endl;
				points2a2plusProches->push_back(pointCour);
				points2a2plusProches->push_back(pointPlusProche);
				// d->drawLine(pointCour, pointPlusProche , 1.0 , 1.0 , 1.0);
			}
			// cout<<"Maillage::voxelDistances() : drawLine  done"<<endl;
		}
		else
		{
			cout<<"Point sans voisin"<<endl;
			d->drawPoint(pointCour , 1 , 51.0 / 255.0 , 204.0 / 255.0);
		}
	}	
}
// void Maillage::testVoisins(Draw *d , vector <>)

vector<Vector*>* Maillage::getToutLesPointsDeMaillages(vector<vector<Figure2D*> *> * maillages)
{	
	map <unsigned long long int , Vector*> *allpointsSansDoublons= new map <unsigned long long int , Vector*>();


	vector<Vector*>* allpoints = new vector<Vector*>();
	for(unsigned int i = 0 ; i < maillages->size() ; i++ )
	{
		for(unsigned int j = 0 ; j < maillages->at(i)->size() ; j++)
		{
			for(unsigned int k = 0 ; k < maillages->at(i)->at(j)->getNombrePoints() ; k ++)
			{
				allpointsSansDoublons->insert(pair<unsigned long long int , Vector*>(maillages->at(i)->at(j)->getPoint(k)->getId(),maillages->at(i)->at(j)->getPoint(k)));
			}
		}
	}


    for( map <unsigned long long int , Vector*>::iterator it = allpointsSansDoublons->begin(); it != allpointsSansDoublons->end(); ++it )
    {
        allpoints->push_back( it->second );
    }
	
	delete allpointsSansDoublons;
	return allpoints;
}