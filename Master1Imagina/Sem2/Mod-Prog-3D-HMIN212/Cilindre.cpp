#include "Cilindre.hpp"
unsigned int nb_visible_surfaces=1;

Cilindre::Cilindre(Base *base , double rayon , double hauteur , unsigned int nb_parall , unsigned int nb_merid )
{
	
	
	

	init_new(nb_parall , nb_merid);
	init_valeurs( base , rayon , hauteur ,nb_parall , nb_merid);
}

void Cilindre::init_new(unsigned int nb_parall , unsigned int nb_merid)
{
	this->nb_lig = nb_parall * 5;
	this->nb_col = nb_merid *5;
	points = new Vector * [this->nb_lig];
	for(unsigned int i = 0 ; i < this->nb_lig ; i++)
	{
		points[i]=new Vector[this->nb_col];
	}

	for(int i = 0 ; i < 3 ; i++)
		this->data_trs[i]= new vector <Figure2D *>();


	this->p_nord = new Vector (0 , 0 , 0 );
	this->p_sud  =new Vector (0 , 0 , 0);


	
	


}

void Cilindre::init_valeurs(Base *base , double rayon , double hauteur ,unsigned int nb_parall , unsigned int nb_merid )
{

	this->rayon = rayon ;
	this->nb_parall = nb_parall;
	this->nb_merid = nb_merid;
	this->base = base;
	this->hauteur = hauteur;
	cout<<"nb_parall="<<nb_parall << " nb_merid="<<nb_merid<<" nb_lig="<<nb_lig<<" nb_col="<<nb_col<<endl;
	
	p_nord->setBase(*base);
	p_sud->setBase(*base);

	this->p_nord->setZ(this->hauteur);
	this->p_sud->setZ(this->base->getO().getZ());

	
	this->p_nord->clearFigures2D();
	this->p_sud->clearFigures2D();


	double d_zi = ((1.0/(nb_parall -1) ) * hauteur) ;
	double z_start = hauteur;
	
	double z = z_start;
	


	for(unsigned int i = 0 ; i < nb_parall ; i++)
	{
		
		z = z_start - (d_zi * i);
		for(unsigned int j = 0 ; j < nb_merid ; j++)
		{	
			
			points[i][j].clearFigures2D();
			points[i][j].setX( base->getO().getX() +
							 rayon * cos(((double) j /nb_merid) * (2.0 * M_PI)));
			
			points[i][j].setY( base->getO().getY() +
							 rayon * sin(((double) j /nb_merid) * (2.0 * M_PI)));
			
			points[i][j].setZ( base->getO().getZ() + z);

			points[i][j].setBase(*base);
			


		}
		
		
	}

	
}

void Cilindre::init_trs()
{
	/*

		INIT TRIANGLES SURFACE NORD

	*/
	Triangle *tr = NULL;
	for(unsigned int j = 0 ; j < this->nb_merid - 1; j++)
	{

		tr = new Triangle(get_pp_nord() , &points[0][j] , &points[0][j+1] );
		
		get_pp_nord()->addFigure2D(tr);
		points[0][j+1].addFigure2D(tr);
		points[0][j].addFigure2D(tr);

		tr->calcNorme();

		data_trs[0]->push_back(tr);
	}
	
	tr = new Triangle(get_pp_nord(), &points[0][this->nb_merid - 1 ] , &points[0][0] );
		
		get_pp_nord()->addFigure2D(tr);
		points[0][this->nb_merid - 1 ].addFigure2D(tr);
		points[0][0].addFigure2D(tr);
		
		tr->calcNorme();
		data_trs[0]->push_back(tr);

	/*

		INIT TRIANGLES SURFACE SUD
		
	*/
	for(unsigned int j = 0 ; j < this->nb_merid - 1; j++)
	{
		tr = new Triangle(get_pp_sud() , &points[this->nb_parall - 1][j+1] ,&points[this->nb_parall-1][j] );

		get_pp_sud()->addFigure2D(tr);
		points[this->nb_parall - 1 ][j+1].addFigure2D(tr);
		points[this->nb_parall - 1 ][j].addFigure2D(tr);
		tr->calcNorme();
		data_trs[2]->push_back(tr);
	}

	tr = new Triangle(get_pp_sud() , &points[this->nb_parall - 1][0] , &points[this->nb_parall - 1][this->nb_merid - 1 ] );
		
		get_pp_nord()->addFigure2D(tr);
		points[this->nb_parall][this-> nb_merid - 1 ].addFigure2D(tr);
		points[this->nb_parall][0].addFigure2D(tr);
		tr->calcNorme();
		data_trs[2]->push_back(tr);


	/*

		INIT TRIANGLES SURFACE CORPS

	*/

	for(unsigned int i = 0 ; i < this->nb_parall - 1; i++)
	{
		for(unsigned int j = 0 ; j< this->nb_merid ; j++)
		{
			if(j < this->nb_merid - 1 )
			{	
				tr = new Triangle(&points[i][j+1] , &points[i][j] , &points[i+1][j] );
				tr->calcNorme();
				data_trs[1]->push_back(tr);
				points[i][j+1].addFigure2D(tr) ; 
				points[i][j].addFigure2D(tr) ; 
				points[i+1][j].addFigure2D(tr) ;


				tr=new Triangle( &points[i+1][j+1] , &points[i][j+1] , &points[i+1][j] );
				tr->calcNorme();
				data_trs[1]->push_back(tr);
				points[i+1][j+1].addFigure2D(tr) ; 
				points[i][j+1].addFigure2D(tr) ; 
				points[i+1][j].addFigure2D(tr) ; 

			}
			
			else if (j == this->nb_merid - 1)
			{
				tr = new Triangle(&points[i][0] , &points[i][j] , &points[i+1][j] );
				tr->calcNorme();
				data_trs[1]->push_back(tr);				
				points[i][0].addFigure2D(tr) ; 
				points[i][j].addFigure2D(tr) ; 
				points[i+1][j].addFigure2D(tr) ; 

				tr=new Triangle( &points[i+1][0] , &points[i][0] , &points[i+1][j] );
				tr->calcNorme();
				data_trs[1]->push_back(tr);				
				points[i+1][0].addFigure2D(tr) ;
				points[i][0].addFigure2D(tr) ; 
				points[i+1][j].addFigure2D(tr) ;
			}
		}
	}


}

bool Cilindre::contientRel(Vector &p)
{
	// cout       <<base->getO().getX() - rayon 
	//     << " " << base->getO().getY() - rayon 
	//     << " " << this->get_p_sud().getZ()
	//     <<" ?< ";
	// cout <<(double) p.getXr()<<" "<< (double) p.getYr()<<" "<<(double) p.getZr() 
	// 	<<" ?< ";
	// cout << (base->getO().getXr() + rayon)
	// <<" "<< (base->getO().getYr() + rayon) 
	// <<" "<< (this->get_p_nord().getZr())<<endl;;

	// glPointSize(7);
	// glColor3f(1,1,0);
	// glBegin(GL_POINTS);
	// 	glVertex3f(base->getO().getX() + rayon,
	// 			   base->getO().getY() + rayon,
	// 			   this->get_p_nord().getZ());

	// 	glVertex3f(base->getO().getX() - rayon,
	// 			   base->getO().getY() - rayon,
	// 			   this->get_p_sud().getZ());
	// glEnd();
		
	// glPointSize(7);
	// glColor3f(0,1,1);
	// glBegin(GL_POINTS);
	// 	glVertex3f(p.getXr() ,
	// 			   p.getYr() ,
	// 			   this->get_p_nord().getZr());

	// 	glVertex3f(base->getO().getXr() + rayon,  		
	// 		base->getO().getYr() + rayon,
	// 		this->get_p_nord().getZr() );
	// glEnd(); 
	return 	p.getXr() <= base->getO().getXr() + rayon &&  		
			p.getYr() <= base->getO().getYr() + rayon &&
			p.getZr() <= this->get_p_nord().getZr() && 

 			p.getXr() >= base->getO().getXr() - rayon  && 
			p.getYr() >= base->getO().getYr() - rayon  &&
			p.getZr() >= this->get_p_sud().getZr() &&
			((pow(p.getXr(),2.0) + pow(p.getYr(),2.0)) <= pow(rayon,2.0)) ;
}

Vector* Cilindre::get_pp_nord()
{
	return this->p_nord;
}

Vector* Cilindre::get_pp_sud()
{
	return this->p_sud;
}


Vector Cilindre::get_p_nord()
{
	return Vector (0 , 0 , this->hauteur, base1);
}

Vector Cilindre::get_p_sud()
{
	return Vector (0 , 0 , this->base->getO().getZ(), base1);
}


void Cilindre::affiche_parall_lignes_corps(Draw *d)
{
	for(unsigned int i = 0 ; i < nb_parall ; i++)
	{

		d->drawCurve(points[i] , nb_merid , 0.0 , 0.3 , 1.0 );
		d->drawLine(&points[i][nb_merid - 1 ] , &points[i][0] , 0.0 , 0.3 , 1.0);
	}
}


void Cilindre::affiche_merid_lignes_nord(Draw *d)
{
	
	Vector point_nord= this->get_p_nord();

	for(unsigned int j = 0 ; j < nb_merid ; j++)
	{
		d->drawLine(&point_nord , &points[0][j] , 0.0 , 1.0 , 0.0);
	}
}

void Cilindre::affiche_merid_lignes_corps(Draw * d)
{
	for(unsigned int j = 0 ; j < nb_merid  ; j++)
	{
		for(unsigned int i =0 ; i< nb_parall -1 ; i++)
		{
			d->drawLine(&points[i][j] , &points[(i+1) % nb_parall][j] , 0.0 , 0.6 , 1.0);
		}
	}
}

void Cilindre::affiche_merid_lignes_sud(Draw *d)
{
	
	Vector point_sud = this->get_p_sud();
	
	 		
	for(unsigned int j = 0 ; j < nb_merid ; j++)
	{
		d->drawLine(&point_sud , &points[nb_parall - 1][j] ,1.0 , 0.0 , 0.0);
	}
}

void Cilindre::affiche_surface_nord(Draw *d)
{
	Vector point_nord =this->get_p_nord();
	d->drawTriangleFan(nb_merid, &point_nord , points[0] , 1);
	
}

void Cilindre::affiche_surface_sud(Draw *d )
{
	Vector point_sud = this->get_p_sud();
	d->drawTriangleFan(nb_merid , &point_sud , points[nb_parall - 1] , -1);

}

void Cilindre::affiche_surface_corps(Draw *d)
{
	for(unsigned int i = 0 ; i < nb_parall ; i ++ )
	{
		for(unsigned int j = 0 ; j < nb_merid ; j ++)
		{
			d->drawSurface(&points[i][j] , &points[i][(j+1) % nb_merid] , &points[(i+1) % nb_parall][(j+1) % nb_merid] , &points[(i+1) % nb_parall][j]);

		}
	}
}



void Cilindre::set_nb_parall_merid(unsigned int nb_parall , unsigned int nb_merid)
{
	unsigned new_nb_lig , new_nb_col;
	new_nb_lig = this->nb_lig;
	new_nb_col = this->nb_col;
	if(nb_parall > nb_lig)
	{
		
		new_nb_lig = nb_parall * 5;
		
	}
	if(nb_merid > nb_col)
	{
		new_nb_col = nb_merid * 5;
	}

	if(nb_parall > nb_lig || new_nb_col > nb_col)
	{
		cout<<"readaptation"<<endl;
		Vector ** new_tab_points = new Vector *[new_nb_lig];;
		
		for(unsigned int i = 0 ; i < new_nb_lig ; i ++)
		{
			new_tab_points[i] = new Vector[new_nb_col];
		}
		
		//liberation de mémoire
		for(unsigned int i = 0 ; i < nb_lig ; i++)
		{
			// delete points[i];
		}
		// delete points;

		points = new_tab_points;
		this->nb_lig 	= new_nb_lig ;
		this->nb_col 	= new_nb_col ;
	}

	cout<<"init valeurs"<<endl;
	init_valeurs(this->base,rayon , hauteur , nb_parall , nb_merid);

	// TODO gestion de fuite mémoire 
	// Il faut delete le contenue pointé par chaque pointeur appartenant à
	// data_trs;
	for(int i = 0 ; i < 3 ; i ++){
		data_trs[i]->clear();
		
	}
	init_trs();

}

void Cilindre::add_merid()
{
	this->set_nb_parall_merid(this->nb_parall , this->nb_merid + 1);
}

void Cilindre::rm_merid()
{
	if(this->nb_merid - 1 >= 0)
		this->set_nb_parall_merid(this->nb_parall , this->nb_merid - 1);
}


void Cilindre::add_parall()
{
	this->set_nb_parall_merid(this->nb_parall + 1 , this->nb_merid);
}

void Cilindre::rm_parall()
{
	if(this->nb_parall  - 1> 0 )
		this->set_nb_parall_merid(this->nb_parall - 1 , this->nb_merid);
}


void Cilindre::affiche_tr_surface_corps(Draw *d)
{
	unsigned int size_of_data_vertex = (this->nb_parall * this->nb_merid)* 3;
	unsigned int size_of_data_index  = (((this->nb_parall-1) * (this->nb_merid) )*6);

	GLfloat data_vertex[size_of_data_vertex];
	GLuint data_index[size_of_data_index];

	for (unsigned int i= 0 ; i < size_of_data_index ; i ++)
	{
		data_index[i] = 0.0;
	}
	
	unsigned int i_vertex = 0 , j_vertex=0; 

	for(unsigned int i = 0 ; i < this->nb_parall  ; i ++)
	{
		for(unsigned int j = 0 ; j <this->nb_merid ; j++ )
		{
			// cout<< "("<<i<< "," << j<<")"<<" (" <<i_vertex<<","<<j_vertex<<")"<<" "<<(i_vertex * this->nb_merid) + j_vertex<<" size_of_vertex="<<size_of_data_vertex<<endl;

			data_vertex[j_vertex    ] = (float)points[i][j].getX();

			data_vertex[j_vertex + 1] = (float)points[i][j].getY();
			
			data_vertex[j_vertex + 2] = (float)points[i][j].getZ();
			
			j_vertex+=3;
			// d->drawPoint(&points[i][j]);
		}
		i_vertex++;
	}

	// data_index[0]=0;
	// data_index[1]=1;
	// data_index[2]=( this->nb_merid);

	// data_index[3]= 1 ;
	// data_index[4]=(this->nb_merid)+ 1;
	// data_index[5]=(this->nb_merid); 

	// data_index[6]=1;
	// data_index[7]=2;
	// data_index[8]=(1+ this->nb_merid);

	// data_index[9]= 2 ;
	// data_index[10]=(1+this->nb_merid)+ 1;
	// data_index[11]=(1+this->nb_merid); 
	unsigned int i_index=0;
	for(unsigned int i = 0 ; i_index < size_of_data_index /*- (size_of_data_index - (nb_visible_surfaces*6))*/	; i++)
	{
		if( (i + 1) % this->nb_merid == 0 )
		{
			data_index[i_index  ]=i;
			data_index[i_index+1]=i+1 - this->nb_merid;
			data_index[i_index+2]=i+this->nb_merid;

			data_index[i_index+3]=i+1 - this->nb_merid;
			data_index[i_index+4]=i+1 ;
			data_index[i_index+5]=(i+this->nb_merid); 
		}
		else
		{
			data_index[i_index  ]=i;
			data_index[i_index+1]=i+1;
			data_index[i_index+2]=(i+this->nb_merid);

			data_index[i_index+3]= i + 1 ;
			data_index[i_index+4]=(i+this->nb_merid)+ 1;
			data_index[i_index+5]=(i+this->nb_merid); 
		}
		i_index+= 6;	
		
	}
	d->drawTrianglesVbo(data_vertex, data_index , sizeof(data_vertex) , sizeof(data_index));

}

void Cilindre::affiche_tr_surface_nord(Draw *d)
{
	GLfloat data_vertex [(this->nb_merid + 1 )*3];
	GLuint data_index[this->nb_merid* 3];
	Vector p_nord = get_p_nord();
	data_vertex[0] = p_nord.getX();
	data_vertex[1] = p_nord.getY();
	data_vertex[2] = p_nord.getZ();
	


	unsigned int i_vertex = 0 , j_vertex=0; 
	for(unsigned int i = 0 ; i < 1 ; i ++)
	{
		for(unsigned int j = 0 ; j < this->nb_merid ; j++ )
		{

			data_vertex[(i_vertex * this->nb_merid) + j_vertex + 3 ] = (float)points[i][j].getX();

			data_vertex[(i_vertex * this->nb_merid) + j_vertex + 4  ] = (float)points[i][j].getY();
			
			data_vertex[(i_vertex * this->nb_merid) + j_vertex + 5  ] = (float)points[i][j].getZ();				
			j_vertex +=3;
		}
		i_vertex++;
	}

	unsigned int i_index=0;
	for(unsigned int i = 0; i < this->nb_merid ; i ++)
	{
		if(i < this->nb_merid -1)
		{
			data_index[i_index]   = 0;
			data_index[i_index+1] = i + 1;
			data_index[i_index+2] = i + 2;
		}
		else 
		{
			data_index[i_index]   = 0;
			data_index[i_index+1] = this->nb_merid ;
			data_index[i_index+2] = 1;	
		}

		i_index+=3;
	}

	// data_index[i_index]   = 0;
	// data_index[i_index+1] = 1;
	// data_index[i_index+2] = 2;
		



	// d->drawPoint(&p_nord);

	d->drawTrianglesVbo(data_vertex, data_index , sizeof(data_vertex) , sizeof(data_index));


}

void Cilindre::affiche_tr_surface_sud(Draw *d)
{
	GLfloat data_vertex [(this->nb_merid + 1 )*3];
	GLuint data_index[this->nb_merid* 3];
	Vector p_sud = get_p_sud();
	data_vertex[0] = p_sud.getX();
	data_vertex[1] = p_sud.getY();
	data_vertex[2] = p_sud.getZ();
	
	unsigned int j_vertex=0; 
	for(int j = this->nb_merid - 1 ; j >= 0 ; j-- )
	{
		
		data_vertex[j_vertex + 3 ] = (float)points[this->nb_parall - 1][j].getX();

		data_vertex[j_vertex + 4  ] = (float)points[this->nb_parall - 1][j].getY();
		
		data_vertex[j_vertex + 5  ] = (float)points[this->nb_parall - 1][j].getZ();				
		j_vertex +=3;
	}
	
	

	unsigned int i_index=0;
	for(unsigned int i = 0; i < this->nb_merid ; i ++)
	{
		if(i < this->nb_merid -1)
		{
			data_index[i_index]   = 0;
			data_index[i_index+1] = i + 1;
			data_index[i_index+2] = i + 2;
		}
		else 
		{
			data_index[i_index]   = 0;
			data_index[i_index+1] = this->nb_merid ;
			data_index[i_index+2] = 1;	
		}

		i_index+=3;
	}

	// data_index[i_index]   = 0;
	// data_index[i_index+1] = 1;
	// data_index[i_index+2] = 2;
		



	// d->drawPoint(&p_sud);

	d->drawTrianglesVbo(data_vertex, data_index , sizeof(data_vertex) , sizeof(data_index));


}


void Cilindre::affiche_tr_all(Draw *d)
{
	// Corp 
	// unsigned int size_of_data_vertex = (this->nb_parall * this->nb_merid)* 3;
	// unsigned int size_of_data_index  = (((this->nb_parall-1) * (this->nb_merid) )*6);


	// nord + sud 
	// data_vetex+= (this->nb_merid + 1 )*3 * 2;
	// data_index+= this->nb_merid* 3 * 2;

}

void Cilindre::affiche_tr_normes_nord(Draw *d)
{
	for(unsigned int i = 0 ; i < data_trs[0]->size(); i ++)
	{
		data_trs[0]->at(i)->drawNorme(d);
		// data_trs[0]->at(i)->drawTrLignes(d);
	}
}

void Cilindre::spotAngleDiedre(float angleSeuil , Draw *d)
{
	for(int k = 0 ; k < 3 ; k++)
	{
		for(unsigned int i = 0 ; i < data_trs[k]->size() ; i++)
		{
			vector <Figure2D*>* voisins = data_trs[k]->at(i)->getVoisins();
			if (voisins == NULL)
			{
				cerr<<"warning ! spotAngleDiedre: voisins == NULL"<<endl;
				return;
			}

			for(unsigned int j = 0 ; j < voisins->size() ; j ++)
			{
				float angleDiedreAvVoisin = data_trs[k]->at(i)->calcAngleDiedre(voisins->at(j)) * 180/M_PI;
				cout<<"angle dièdre "<< angleDiedreAvVoisin<<endl;
				if(angleDiedreAvVoisin < angleSeuil )
				{
					
					data_trs[k]->at(i)->drawTrLignes(d , 1.0 , 1.0 , 0.0 );
				}
				else
				{
					data_trs[k]->at(i)->drawTrLignes(d);
				}
			}
		}	
	}
	
}
	


void Cilindre::affiche_tr_normes_sud(Draw *d)
{
	for(unsigned int i = 0 ; i < data_trs[2]->size(); i ++)
	{
		data_trs[2]->at(i)->drawNorme(d);
		data_trs[2]->at(i)->drawTrLignes(d);
	}	
}

void Cilindre::affiche_tr_normes_corps(Draw *d)
{
	for(unsigned int i = 0 ; i < data_trs[1]->size(); i ++)
	{
		data_trs[1]->at(i)->drawNorme(d);
		data_trs[1]->at(i)->drawTrLignes(d);
	}
}

void Cilindre::affiche_triangles_test1(Draw *d)
{
	GLfloat data_vertex[4 * 3] = {
      (float)base1.getO().getX(), (float)base1.getO().getY(), (float)base1.getO().getZ(),
      (float)base1.getVi().getX(), (float)base1.getVi().getY(), (float)base1.getVi().getZ(),
      (float)base1.getVj().getX(), (float)base1.getVj().getY(), (float)base1.getVj().getZ(),
      (float)base1.getVk().getX(), (float)base1.getVk().getY(), (float)base1.getVk().getZ()
    };

	   GLuint data_index[6] = {
      0,1,2,0,1,3
    };

	
	d->drawTrianglesVbo(data_vertex, data_index , sizeof(data_vertex) , sizeof(data_index));


}

