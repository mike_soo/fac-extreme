#include "BezierCurveByBernstein.hpp"

/*
	À demander dans le cours : 
	avec n = 4 et u = 2
	(2 + (1 - 2 ) ^4 ! = sum i=0 to 4 (Binomial [4,i] * 2 ^i * (1 - 2)^(4 - i) )

	Pourtant dans le poly page 24 c'est marqué que cela devrait être égal.
*/

BezierCurveByBernstein::BezierCurveByBernstein(Vector * tabControlPoint,Vector* tabCurveByBernstein,long nbControlPoint, long nbU)
{
	this->tabControlPoint = tabControlPoint;
	this->tabCurveByBernstein = tabCurveByBernstein;
	this->nbControlPoint = nbControlPoint;
	this->nbU = nbU - 1 ;
	this->tabCurveByCasteljau = new Vector*[nbControlPoint];

	this->tabCurveByCasteljau[0]=this->tabControlPoint;
	cout<<"BezierCurbeByBernstein::BezierCurveByBernstein() : init"<<endl;
	for (int k = 1 ; k < nbControlPoint ; k++)
	{
		tabCurveByCasteljau[k]= new Vector[nbControlPoint - k];
	} 
}

void BezierCurveByBernstein::setPointsControl(Vector * tabControlPoint,Vector* tabCurveByBernstein,long nbControlPoint, long nbU)
{
	this->tabControlPoint = tabControlPoint;
	this->tabCurveByBernstein = tabCurveByBernstein;
	this->nbControlPoint = nbControlPoint;
	this->nbU = nbU - 1 ;
	this->tabCurveByCasteljau = new Vector*[nbControlPoint];

	this->tabCurveByCasteljau[0]=this->tabControlPoint;
	for (int k = 1 ; k < nbControlPoint ; k++)
	{
		tabCurveByCasteljau[k]= new Vector[nbControlPoint - k];
	}  

}

void BezierCurveByBernstein::setPointsControl(Vector tabControlPoint[],long nbControlPoint)
{
	this->tabControlPoint = new Vector[nbControlPoint];
	
	for (int i ; i < nbControlPoint ; i++)
	{
		this->tabControlPoint[i] = tabControlPoint[i];
	}
	this->tabCurveByBernstein = tabCurveByBernstein;
	this->nbControlPoint = nbControlPoint;
	this->nbU = nbU - 1 ;
	this->tabCurveByCasteljau = new Vector*[nbControlPoint];

	this->tabCurveByCasteljau[0]=this->tabControlPoint;

	cout<<"BezierCurveByBernstein::setPointsControl"<<endl;

	for(int i= 0 ; i < this->nbControlPoint ; i++)
	{
		cout<<"("<<tabCurveByCasteljau[0][i].getX()<<","<<tabCurveByCasteljau[0][i].getY()<<","<<
				tabCurveByCasteljau[0][i].getZ()<<")"<<endl;
	}

	for (int k = 1 ; k < nbControlPoint ; k++)
	{
		tabCurveByCasteljau[k]= new Vector[nbControlPoint - k];
	}  

}


void BezierCurveByBernstein::calcDataCurveByBernstein()
{
	for (long u = 0 ; u <= nbU ; u++ )
	{
		tabCurveByBernstein[u].setX(0);
		tabCurveByBernstein[u].setY(0);
		for (long i = 0 ; i < nbControlPoint; i++)
		{
			tabCurveByBernstein[u] += B(nbControlPoint - 1 , i , (double) u / nbU) * tabControlPoint[i] ;
		}


		// cout<<"("<<tabCurveByBernstein[u].getX()<<","<<tabCurveByBernstein[u].getY()<<")"<<endl;
		
	}



}

void BezierCurveByBernstein::calcDataBezierCurveByCasteljau(double u)
{
	for (int k = 1 ; k < nbControlPoint /*(niveau max)*/ ; k++)
	{
		for (int i = 0 ; i < nbControlPoint - k ; i++)
		{
			tabCurveByCasteljau[k][i] = 
				(1.0 - u)* tabCurveByCasteljau[k-1][i]
				+ (u * tabCurveByCasteljau[k-1][i+1]);  
				
		}
		
	}
}

Vector BezierCurveByBernstein::pByCasteljau(double u ,int k , int i )
{
	if(k == 0)
	{
		return this->tabCurveByBernstein[i];
	}
	// if( i == (nbControlPoint - 1) - k )
	// {
		
	// }
	else if( i < this->nbU - 1 )
	{
		// printf("u = %f ; k = %i ; i = %i\n",u,k,i);
		return ((1.0 - u) * pByCasteljau ( u , k - 1 , i ))  
					+  u  *  pByCasteljau(u , k -1 , i + 1 );
	}
	else
	{
		
		return Vector();
	}
}



double BezierCurveByBernstein::B( int n ,  int i , double u)
{
	return (double) binomialCoeff(n,i) * pow(u, i) * pow( 1.0 - u ,n - i );
}

void BezierCurveByBernstein::drawCurveByBernstein(Draw *d)
{
	d->setPoints(tabCurveByBernstein,nbU + 1);
	d->drawCurve();
}

void BezierCurveByBernstein::drawCurveByCasteljau(Draw *d)
{
	cout<<"BezierCurveByBernstein::drawCurveByCasteljau"<<endl;

	for(int i = 0 ; i < nbControlPoint ; i++)
	{	
		for(int j = 0 ; j < nbControlPoint - i ; j++)
		{
			if(i == 0)
			{
				glColor3f(0,0,1.0);
			}
			else
			{
				glColor3f(0,1.0,0);
			}
			// cout<<"("<<tabCurveByCasteljau[i][j].getX()<<","<<tabCurveByCasteljau[i][j].getY()<<","<<tabCurveByCasteljau[i][j].getZ()<<")"<<endl;
			
			tabCurveByCasteljau[i][j].setBase(base1);
			d->drawPoint(&tabCurveByCasteljau[i][j]);
			
		}

		d->setPoints(tabCurveByCasteljau[i] , nbControlPoint - i);
		d->drawCurve();
		// d->drawCurve(&tabCurveByCasteljau[i] ,nbControlPoint - i , base1);

	}


	for(int i = 0 ; i < nbControlPoint ; i++)
	{	
		for(int j = 0 ; j < nbControlPoint - i ; j++)
		{
			
			tabCurveByCasteljau[i][j].setBase(base0);	
		}
	}


	
}

Vector* BezierCurveByBernstein::getPointCurveCasteljau()
{
	// cout<<"BezierCurveByBernstein::getPointCurveCasteljau"<<endl;
	// cout<<" BCurveBBernstion : " << (tabCurveByCasteljau[nbControlPoint-1][0].getX())<<" , "<<tabCurveByCasteljau[nbControlPoint-1][0].getY()<<" , "<<tabCurveByCasteljau[nbControlPoint-1][0].getZ()<<endl; 
	
	return &tabCurveByCasteljau[nbControlPoint - 1][0];
}
