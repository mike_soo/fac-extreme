#ifndef Cilindre_HPP
#define Cilindre_HPP
#include "Vector.hpp"
#include "Draw.hpp"
#include "Triangle.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <vector>
extern Base base0;
extern Base base1;

class Cilindre {

private:
	Vector centre;
	double rayon;
	double hauteur;
	Vector **points;
	Base *base;
	unsigned int nb_parall;
	unsigned int nb_merid;
	unsigned int nb_lig;
	unsigned int nb_col;
	vector <Figure2D*>* data_trs[3];
	Vector * p_nord;
	Vector * p_sud;
public:
	Cilindre(Base *base , double rayon , double hauteur , unsigned int nb_parall , unsigned int nb_merid );
	void affiche_parall_lignes_corps(Draw *d);
	void affiche_merid_lignes_corps(Draw * d);
	void affiche_surface_corps(Draw *d);
	void affiche_merid_lignes_nord(Draw *d);
	void affiche_merid_lignes_sud(Draw *d);
	void set_nb_parall_merid(unsigned int nb_parall , unsigned int nb_merid);
	void init_new(unsigned int nb_parall , unsigned int nb_merid);
	void init_valeurs(Base *base , double rayon , double hauteur ,unsigned int nb_parall , unsigned int nb_merid );
	void init_valeurs(Base *base , double rayon , unsigned int nb_parall , unsigned int nb_merid );
	Vector get_p_nord();
	Vector get_p_sud();
	Vector* get_pp_nord();
	Vector* get_pp_sud();
	void add_merid();
	void rm_merid();
	void add_parall();
	void rm_parall();
	void affiche_surface_nord(Draw *d);
	void affiche_surface_sud(Draw *d);
	bool contientRel(Vector &p);

	void affiche_tr_surface_corps(Draw *d);
	void affiche_tr_surface_nord(Draw *d);
	void affiche_tr_surface_sud(Draw *d);
	void affiche_triangles_test1(Draw *d);

	void affiche_tr_normes_nord(Draw *d);
	void affiche_tr_normes_sud(Draw *d);
	void affiche_tr_normes_corps(Draw *d);
	void init_trs();
	void spotAngleDiedre(float angleSeuil , Draw *d);
	void affiche_tr_all(Draw *d);
};	
#endif