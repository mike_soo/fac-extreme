#ifndef BEZIERCURVEBYBERNSTEIN_H
#define BEZIERCURVEBYBERNSTEIN_H
#include <cmath>
#include "Math.hpp"
#include "Vector.hpp"
#include "Draw.hpp"

extern Base base0;
extern Base base1;
class BezierCurveByBernstein
{

private:
	Vector * tabControlPoint;
	Vector * tabCurveByBernstein;
	long nbControlPoint;
	long nbU;
	Vector ** tabCurveByCasteljau;
public:
	BezierCurveByBernstein(Vector * tabControlPoint,Vector* tabCurveByBernstein,long nbControlPoint, long nbU);
	// ~BezierCurveByBernstein();
	void setPointsControl(Vector * tabControlPoint,Vector* tabCurveByBernstein,long nbControlPoint, long nbU);
	void setPointsControl(Vector tabControlPoint[],long nbControlPoint);
	void calcDataCurveByBernstein();
	void calcDataBezierCurveByCasteljau();
	void calcDataBezierCurveByCasteljau(double u);
	Vector pByCasteljau(double u ,int k , int i );
	void drawCurveByBernstein(Draw *d);
	double B( int n , int i , double u);
	void drawCurveByCasteljau(Draw *d);
	Vector* getPointCurveCasteljau();
};	



#endif