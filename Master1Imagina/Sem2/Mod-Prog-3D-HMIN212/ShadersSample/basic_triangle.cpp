#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <iostream>

void resize(GLFWwindow* window, int width, int height);

const char *vertexShaderSource = "#version 330 core\n"
  "layout (location = 0) in vec3 aPos;\n"
  "void main()\n"
  "{\n"
  "   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
  "}\0";

const char *fragmentShaderSource = "#version 330 core\n"
  "out vec4 FragColor;\n"
  "void main()\n"
  "{\n"
  "   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
  "}\0";

int main(int, char**)
{
  if (!glfwInit()) {
    std::cout << "glfw init error" << std::endl;
    exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow(800, 600, "BasicTriangle", nullptr, nullptr);

  if (window == nullptr) {
    std::cout << "Failed to create window." << std::endl;
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, resize);

  int status = glewInit();

  if (status != GLEW_OK) {
    std::cout << "GLEW init error" << std::endl;;
    exit(EXIT_FAILURE);
  }

  int vert_id = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vert_id, 1, &vertexShaderSource, nullptr);
  glCompileShader(vert_id);

  char info[512];
  glGetShaderiv(vert_id, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(vert_id, 512, nullptr, info);
    std::cout << "vert shader compile error" << std::endl;
  }

  int frag_id = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag_id, 1, &fragmentShaderSource, nullptr);
  glCompileShader(frag_id);
  glGetShaderiv(frag_id, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(frag_id, 512, nullptr, info);
    std::cout << "frag shader compile error" << std::endl;
  }

  int prog_id = glCreateProgram();
  glAttachShader(prog_id, vert_id);
  glAttachShader(prog_id, frag_id);
  glLinkProgram(prog_id);
  glGetProgramiv(prog_id, GL_LINK_STATUS, &status);

  if (!status) {
    glGetProgramInfoLog(prog_id, 512, nullptr, info);
    std::cout << "program linking error" << std::endl;
  }

  glDeleteShader(vert_id);
  glDeleteShader(frag_id);

  float vertices[] = {
    -0.5f, -0.5f, 0.0f,
     0.5f, -0.5f, 0.0f,
     0.0f,  0.5f, 0.0f
  };

  unsigned int vbo, vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glGenBuffers(1, &vbo);
  // A buffer object actually is created by binding one of the reserved name
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  // allocates storage for the buffer object
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  glUseProgram(prog_id);

  while (!glfwWindowShouldClose(window)) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      glfwSetWindowShouldClose(window, true);

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glBindVertexArray(vao);
    glDrawArrays(GL_LINE_LOOP, 0, 3);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  glDeleteVertexArrays(1, &vao);
  glDeleteBuffers(1, &vbo);

  glfwTerminate();
  return 0;
}

void resize(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
}
