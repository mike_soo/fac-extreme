#ifndef FIGURE2D_HPP
#define FIGURE2D_HPP
#include "Vector.hpp"
#include "Draw.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <vector>	
using namespace std;
class Vector;
class Draw;
class Figure2D {
private:   
	Vector * norme;

public :
	Figure2D();
	virtual void  calcNorme()=0;
	virtual vector <Figure2D*> * getVoisins()=0;
	virtual Vector * getPoint(int i)=0;
	virtual Vector * getNorme()=0;
	virtual void drawNorme(Draw* d)=0;
	virtual float calcAngleDiedre(Figure2D *fig);
	//TODO : changer drawTrLignes par drawLignes();
	virtual void drawTrLignes(Draw *d)=0;
	virtual void drawTrLignes( Draw *d , float r , float g , float b )=0;
	virtual bool contientPoint(Vector * p)=0;
	virtual unsigned int getNombrePoints()=0;
}; 

#endif

