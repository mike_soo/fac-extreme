#ifndef VECTOR_HPP
#define VECTOR_HPP

#include "Point.hpp"
#include "Figure2D.hpp"
#include "Draw.hpp"

// #include "Triangle.hpp"
#include <iostream> 
#include <vector>
#include <set>
#include <map>
using namespace std;

class VoxelG;
class Base;
class Figure2D;
class Vector {

public:
    Vector();
    Vector(const double x, const double y , const double z);
    Vector(const double x, const double y , const double z , Base& base);
    Vector(const Vector&);

    void initId();

    double norme() const;
    void normalize();
    double scalar(const Vector&) const;
    Vector cross(const Vector&) const;
    void projection(const Vector& b , const Vector& c , Vector& p_proj);
    float distance(Vector * p);
    // repere absolue (par rapport à base R0)
    double getX() const;
    double getY() const;
    double getZ() const;
    // repere relatif (par rapport à base courante)
    double getXr() const; 
    double getYr() const; 
    double getZr() const;

    double getTest()const;
    Base* getBase() const;
    void setBase(Base& base);
    void setX(const double);
    void setY(const double);
    void setZ(const double);

    vector <Figure2D*> * getAllFigures2D();
    void addFigure2D(Figure2D* figure2D);
    void clearFigures2D();
    set<Vector*> * getSommetsCommunsDansTrianglesAdjacents2D(Vector * p);
    Figure2D * getCommonTriangle(vector <Vector*> * points);

    void estDansVoxelG(VoxelG* voxelG);
    VoxelG* getVoxel();
    Vector& operator=(const Vector&);
    Vector& operator+=(const Vector&);
    Vector& operator-=(const Vector&);
    Vector& operator*=(const double);
    Vector& operator/=(const double);

    void affichePoint(Draw * d , float r, float g , float b);
    static unsigned long long int giveId();

    set<unsigned long long int> *idsMaillages;
    map <unsigned long long int, Vector*>* pairsDeSubdivision=NULL;
    map <unsigned long long int, Vector*>* parentsDeSubdivision=NULL;
    vector <Vector*>* parentsDeSubdivisionOrd=NULL;
    static unsigned long long int id;
    unsigned long long int getId() const;
private:
    unsigned long long int myid;
    double _x;
    double _y;
    double _z;
    VoxelG *voxelG;
    Base *base;
    vector <Figure2D*>* figures2D=NULL;
};

Vector operator+(const Vector&, const Vector&);
Vector operator-(const Vector&, const Vector&);
Vector operator*(const Vector&, const double);
Vector operator*(const double, const Vector&);
double operator*( const Vector& vec1 , const Vector& vec2);
Vector operator/(const Vector&, const double);
Vector operator/(const double, const Vector&);
bool operator!=(const Vector& , const Vector&);
bool operator==(const Vector& , const Vector&);
ostream& operator<<(ostream& os, const Vector& v) ; 
#endif
