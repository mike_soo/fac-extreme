#include "Voxel.hpp"

Voxel::Voxel(Base *base , Vector origine_rel ,double longueur )
{
	init_valeurs( base ,origine_rel ,longueur );
	this->p1 =Vector( origine_rel.getXr()            , origine_rel.getYr() + longueur , origine_rel.getZr()  	       , *base);
	this->p2 =Vector( origine_rel.getXr() + longueur , origine_rel.getYr() + longueur , origine_rel.getZr() 	       , *base);
	this->p3 =Vector( origine_rel.getXr() + longueur , origine_rel.getYr()  		  , origine_rel.getZr()            , *base);
	
	this->p4 =Vector( origine_rel.getXr()  		     , origine_rel.getYr() 			  , origine_rel.getZr() + longueur , *base);
	this->p5 =Vector( origine_rel.getXr() 			 , origine_rel.getYr() + longueur , origine_rel.getZr() + longueur , *base);
	this->p6 =Vector( origine_rel.getXr() + longueur , origine_rel.getYr() + longueur , origine_rel.getZr() + longueur , *base);
	this->p7 =Vector( origine_rel.getXr() + longueur , origine_rel.getYr() 			  , origine_rel.getZr() + longueur , *base);
}

Voxel::Voxel(Base *base , Vector* origine , double longeur)
{
	init_valeurs(base, origine , longeur);
	this->pointsVoxel = new vector <Vector*> ();
	Vector * pVoxel;
	

	pVoxel = new Vector( origine->getX() , origine->getY() , origine->getZ() , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX()            , origine->getY() + longueur , origine->getZ()  	       , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX() + longueur , origine->getY() + longueur , origine->getZ() 	       , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX() + longueur , origine->getY()  		  , origine->getZ()            , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX()  		     , origine->getY() 			  , origine->getZ() + longueur , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX() 			 , origine->getY() + longueur , origine->getZ() + longueur , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX() + longueur , origine->getY() + longueur , origine->getZ() + longueur , *base);
	this->pointsVoxel->push_back(pVoxel);
	
	pVoxel = new Vector( origine->getX() + longueur , origine->getY() 			  , origine->getZ() + longueur , *base);
	this->pointsVoxel->push_back(pVoxel);
}

void Voxel::draw(Draw *d)
{
	d->drawSurface(&origine_rel , &p1 , &p2 , &p3, 1.0 , 0.0 , 0.0); //face
	d->drawSurface(&p4 , &p7 , &p6 , &p5, 1.0 , 0.0 , 0.0);	//arriere
	d->drawSurface(&p2 , &p6 , &p7 , &p3, 1.0 , 0.0 , 0.0); //droite
	d->drawSurface(&p5 , &p1 , &origine_rel , &p4, 1.0 , 0.0 , 0.0); //gauche
	d->drawSurface(&p5 , &p6 , &p2 , &p1, 1.0 , 0.0 , 0.0); //dessus
	d->drawSurface(&p4 , &origine_rel , &p3 , &p7, 1.0 , 0.0 , 0.0);	//dessous

	//face
	d->drawLine(&p1 , &p2 , 0.1 , 0.5 ,0.2);
	d->drawLine(&p2 , &p3, 0.1 , 0.5 ,0.2);
	d->drawLine(&p3 , &origine_rel, 0.1 , 0.5 ,0.2);
	d->drawLine(&origine_rel , &p1, 0.1 , 0.5 ,0.2);

	//arriere
	d->drawLine(&p5 , &p6, 0.1 , 0.5 ,0.2);
	d->drawLine(&p6 , &p7, 0.1 , 0.5 ,0.2);
	d->drawLine(&p7 , &p4, 0.1 , 0.5 ,0.2);
	d->drawLine(&p4 , &p5, 0.1 , 0.5 ,0.2);

	//left
	d->drawLine(&p1 , &p5, 0.1 , 0.5 ,0.2);
	d->drawLine(&p5 , &p4, 0.1 , 0.5 ,0.2);
	d->drawLine(&p4 , &origine_rel, 0.1 , 0.5 ,0.2);
	d->drawLine(&origine_rel , &p1, 0.1 , 0.5 ,0.2);
	
	//right		
	d->drawLine(&p2 , &p6, 0.1 , 0.5 ,0.2);
	d->drawLine(&p6 , &p7, 0.1 , 0.5 ,0.2);
	d->drawLine(&p7 , &p3, 0.1 , 0.5 ,0.2);
	d->drawLine(&p3 , &p2, 0.1 , 0.5 ,0.2);

	//dessus
	d->drawLine(&p5,&p6, 0.1 , 0.5 ,0.2);
	d->drawLine(&p6,&p2, 0.1 , 0.5 ,0.2);
	d->drawLine(&p2,&p1, 0.1 , 0.5 ,0.2);
	d->drawLine(&p1,&p5, 0.1 , 0.5 ,0.2);

	//dessou
	d->drawLine(&origine_rel,&p4, 0.1 , 0.5 ,0.2);
	d->drawLine(&p4,&p7, 0.1 , 0.5 ,0.2);
	d->drawLine(&p7,&p3, 0.1 , 0.5 ,0.2);
	d->drawLine(&p3,&origine_rel, 0.1 , 0.5 ,0.2);

		
}



void Voxel::draw_surfaces(Draw *d)
{
	d->drawSurface(&origine_rel , &p1 , &p2 , &p3, 1.0 , 0.0 , 0.0); //face
	d->drawSurface(&p4 , &p7 , &p6 , &p5, 1.0 , 0.0 , 0.0);	//arriere
	d->drawSurface(&p2 , &p6 , &p7 , &p3, 1.0 , 0.0 , 0.0); //droite
	d->drawSurface(&p5 , &p1 , &origine_rel , &p4, 1.0 , 0.0 , 0.0); //gauche
	d->drawSurface(&p5 , &p6 , &p2 , &p1, 1.0 , 0.0 , 0.0); //dessus
	d->drawSurface(&p4 , &origine_rel , &p3 , &p7, 1.0 , 0.0 , 0.0);
}

void Voxel::draw_lines(Draw *d ,float r , float g , float b)
{
	d->drawLine(&p1 , &p2 , r , g , b);
	d->drawLine(&p2 , &p3 , r , g , b);
	d->drawLine(&p3 , &origine_rel , r , g , b);
	d->drawLine(&origine_rel , &p1 , r , g , b);
	//arriere
	d->drawLine(&p5 , &p6 , r , g , b);
	d->drawLine(&p6 , &p7 , r , g , b);
	d->drawLine(&p7 , &p4 , r , g , b);
	d->drawLine(&p4 , &p5 , r , g , b);

	//left
	d->drawLine(&p1 , &p5 , r , g , b);
	d->drawLine(&p5 , &p4 , r , g , b);
	d->drawLine(&p4 , &origine_rel , r , g , b);
	d->drawLine(&origine_rel , &p1 , r , g , b);
	
	//right		
	d->drawLine(&p2 , &p6 , r , g , b);
	d->drawLine(&p6 , &p7 , r , g , b);
	d->drawLine(&p7 , &p3 , r , g , b);
	d->drawLine(&p3 , &p2 , r , g , b);

	//dessus
	d->drawLine(&p5,&p6 , r , g , b);
	d->drawLine(&p6,&p2 , r , g , b);
	d->drawLine(&p2,&p1 , r , g , b);
	d->drawLine(&p1,&p5 , r , g , b);

	//dessou
	d->drawLine(&origine_rel,&p4 , r , g , b);
	d->drawLine(&p4,&p7 , r , g , b);
	d->drawLine(&p7,&p3 , r , g , b);
	d->drawLine(&p3,&origine_rel , r , g , b);

}

void Voxel::init_valeurs(Base *base ,Vector origine_rel , double longueur)
{
	this->base = base;
	this->origine_rel = origine_rel ;
	this->longueur = longueur;
}

void Voxel::init_valeurs(Base * base , Vector *origine , double longuer)
{
	this->base = base;
	this->origine = origine;
	this->longueur=longueur;
}

void Voxel::octree(Sphere *s , Cilindre *c , int res , int res_max , Draw *d , bool go_next , int mode)
{
	
	Vector *tab_point_voxel[8];
	tab_point_voxel[0] = &origine_rel;
	tab_point_voxel[1] = &p1;
	tab_point_voxel[2] = &p2;
	tab_point_voxel[3] = &p3;
	tab_point_voxel[4] = &p4;
	tab_point_voxel[5] = &p5;
	tab_point_voxel[6] = &p6;
	tab_point_voxel[7] = &p7;
	

	/*  next voxel ===============================> */
	Vector origine_rel_centree = Vector(origine_rel);

	Voxel* tab_next_points[8];
	
	Voxel p0next=Voxel(base , origine_rel , longueur / 2.0 );
	tab_next_points[0] = &p0next;
	
	origine_rel_centree = Vector(origine_rel);	
	origine_rel_centree.setY(origine_rel_centree.getYr() + (longueur / 2.0));
	Voxel p1next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[1] = &p1next;
	
	origine_rel_centree = Vector(origine_rel);
	origine_rel_centree.setY(origine_rel_centree.getYr() + (longueur / 2.0));
	origine_rel_centree.setX(origine_rel_centree.getXr() + (longueur / 2.0));
	Voxel p2next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[2] = &p2next;
	
	origine_rel_centree = Vector(origine_rel);
	origine_rel_centree.setX(origine_rel_centree.getXr() + (longueur / 2.0));
	Voxel p3next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[3] = &p3next;
	
	origine_rel_centree = Vector(origine_rel);
	origine_rel_centree.setZ(origine_rel_centree.getZr() + (longueur / 2.0));
	Voxel p4next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[4] = &p4next;
	
	origine_rel_centree = Vector(origine_rel);
	origine_rel_centree.setY(origine_rel_centree.getYr() + (longueur / 2.0));
	origine_rel_centree.setZ(origine_rel_centree.getZr() + (longueur / 2.0));
	Voxel p5next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[5] = &p5next;
	
	origine_rel_centree = Vector(origine_rel);
	origine_rel_centree.setZ(origine_rel_centree.getZr() + (longueur / 2.0));
	origine_rel_centree.setY(origine_rel_centree.getYr() + (longueur / 2.0));
	origine_rel_centree.setX(origine_rel_centree.getXr() + (longueur / 2.0));
	Voxel p6next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[6] = &p6next;
	
	origine_rel_centree = Vector(origine_rel);
	origine_rel_centree.setZ(origine_rel_centree.getZr() + (longueur / 2.0));
	origine_rel_centree.setX(origine_rel_centree.getXr() + (longueur / 2.0));
	Voxel p7next=Voxel(base , origine_rel_centree , longueur / 2.0 );
	tab_next_points[7] = &p7next;

	/*===============================================<*/
	this->draw_lines(d , 0.0 , 1.0 , 0.0);
	

	for (int i = 0; i < 8 ; i ++)
	{
		bool valid = false;
		//d->drawPoint(tab_point_voxel[i] , 1 , 1 , 0);
		//sphere
		if(mode == 0 )
		{
			valid = s->contient(*tab_point_voxel[i]);
		}
		
		//cilindre
		else if(mode == 1 )
		{
			valid = c->contientRel(*(tab_point_voxel[i]));
			//d->drawPoint(tab_point_voxel[i] , 1.0 , 1.0, 1.0);
		}

		//union
		else if(mode == 2 )
		{
			valid = s->contient(*tab_point_voxel[i]) || c->contientRel(*tab_point_voxel[i]);
		}
		//intersection
		else if(mode == 3 )
		{
			valid = s->contient(*tab_point_voxel[i]) && c->contientRel(*tab_point_voxel[i]);
		}

		//somme cercle - cilindre
		else if(mode == 4)
		{
			valid = s->contient(*tab_point_voxel[i]) && !c->contientRel(*tab_point_voxel[i]);
		}
		// cilindre - cercle
		else if(mode == 5)
		{
			valid = !s->contient(*tab_point_voxel[i]) && c->contientRel(*tab_point_voxel[i]);
		}
		
		if (valid || go_next )
		{		
			
			
			if (valid && res>=res_max)
			{
				Voxel v_final(base , origine_rel , longueur);
				// v_final.draw(d);
				v_final.draw_lines(d , 0.0 , 0.0 , 1.0);
				v_final.draw_surfaces(d);
			}

			else if(valid)
			{
				for (int j = 0; i < 8 ; i ++)
				{
					tab_next_points[j]->octree(s , c ,res +1 , res_max , d , true , mode);
				}
				
			}

			else if( go_next && (res <= res_max))
			tab_next_points[i]->octree(s , c , res + 1 , res_max , d , false, mode);
			
		}
		
	}
	
	
}

void Voxel::addPoint(Vector *p)
{
	this->pointsContenus->push_back(p);
}


void Voxel::octree( int res , int res_max , Draw *d )
{

	// Vector *tab_point_voxel[8];
	// tab_point_voxel[0] = &origine_rel;
	// tab_point_voxel[1] = &p1;
	// tab_point_voxel[2] = &p2;
	// tab_point_voxel[3] = &p3;
	// tab_point_voxel[4] = &p4;
	// tab_point_voxel[5] = &p5;
	// tab_point_voxel[6] = &p6;
	// tab_point_voxel[7] = &p7;
	
	this->draw_lines(d , 0.0 , 1.0 , 0.0);

	/*  next voxel ===============================> */
	if(this->points->size()> 0 )
	{
		Vector *origine_centree =NULL;
		
		Voxel *voxelSuivant=new Voxel(base , origine , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);	
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		origine_centree->setY(origine_centree->getYr() + (longueur / 2.0));
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );
		
		origine_centree =new Vector(*origine);
		origine_centree->setZ(origine_centree->getZr() + (longueur / 2.0));
		origine_centree->setX(origine_centree->getXr() + (longueur / 2.0));
		voxelSuivant=new Voxel(base , origine_centree , longueur / 2.0 );
		filsVoxel->push_back ( voxelSuivant );

		for(unsigned int i = 0 ; i < filsVoxel->size() ; i++)
		{
			for(unsigned int j = 0 ; j < points->size(); j++)
			{
				// if(points->at(i) )
			}
		}


		for(unsigned int i = 0 ; i  < filsVoxel->size() ; i ++)
		{
			filsVoxel->at(i)->octree(res , res_max , d);
		}
	}




}



bool Voxel::contientPoint(Vector * p)
{
	if (this->origine->getX())
	{

	}
	return false;	
}