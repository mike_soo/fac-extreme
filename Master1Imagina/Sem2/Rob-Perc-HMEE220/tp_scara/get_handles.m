function [ Joint_Handles ] = get_handles( id, vrep, Joints_Number, Joint_Handle_Name )
%  on r?cupere les handles des moteurs
%  arguments d'entree
    %  id : identificateur de la connexion
    %  vrep : la bibliotheque
    %  Joints_Number :nombre de valeur articulaire ? recuperer
    %  Joint_Handle_Name : nom des articulations
%retour 
    % la valeur des articulations
Joint_Handles=zeros(1,Joints_Number);
for i=1:Joints_Number
    name=strcat(Joint_Handle_Name,num2str(i));
    [~, Joint_Handles(i)] = vrep.simxGetObjectHandle( id, name ,vrep.simx_opmode_oneshot_wait );
end
end

