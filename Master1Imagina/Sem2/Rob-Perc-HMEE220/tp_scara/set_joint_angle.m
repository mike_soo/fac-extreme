function [  ] = set_joint_angle( id, vrep,Joint_Handles, Joints_Number,Joint_Angle_Command, mode)
%% CHOIX DU MODE DE FONCTIONNEMENT

if strcmp(mode,'oneshot')
    opmode=vrep.simx_opmode_oneshot;
elseif strcmp(mode,'buffer')
    opmode=vrep.simx_opmode_buffer;
else
    disp('Error get_joint_angle mode \n')
end

%% MAIN
vrep.simxPauseCommunication( id, 1); % Pour envoyer le paquet en entier
for i = 1:Joints_Number
    error=vrep.simxSetJointPositionœ( id, Joint_Handles(i),Joint_Angle_Command(i),opmode);
    if (error>1) disp('Error SetJointPosition \n'); error
    end
end
vrep.simxPauseCommunication( id, 0);
end

