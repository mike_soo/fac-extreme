globals [continue]
 

turtles-own [
  flockmates         ;; agentset of nearby turtles
  nearest-neighbor   ;; closest one of our flockmates
  randcolor
  my-goal
  bloque
  ctask
]
patches-own [
  goal
  obstacle
  potential
]

to setup
 clear-all
 reset-ticks
 ask patches [
   set goal 0
   set obstacle 0
   set potential 0
]
 
 createObstacles
 createGoals

 create-agents


end

 
to createObstacles
  ask patch 3 0 [set-obstacle]
  ask patch 3 1 [set-obstacle]
  ask patch 3 2 [set-obstacle]
  ask patch 2 2 [set-obstacle]
  ask patch 1 2 [set-obstacle]
  ask patch 0 2 [set-obstacle]
  ask patch -1 2 [set-obstacle]
  ask patch -2 2 [set-obstacle]
  ask patch -3 2 [set-obstacle]
  ask patch -3 1 [set-obstacle]
  ask patch -3 0 [set-obstacle]
  ask patch -3 -1 [set-obstacle]
  ask patch -3 -2 [set-obstacle]
  ask patch -3 -3 [set-obstacle]
  ask patch -3 -4 [set-obstacle]
  ask patch -3 -5 [set-obstacle]
  ask patch -3 -6 [set-obstacle]
  ask patch -3 -7 [set-obstacle]
  ask patch -3 -8 [set-obstacle]
  ask patch -3 -9 [set-obstacle]
  ask patch -4 -9 [set-obstacle]
  ask patch -5 -9 [set-obstacle]

end

to createGoals
  ask patch 0 9 [set-goal]
  ask patch 0 5 [set-goal]
  ask patch -4 -8 [set-goal]
  repeat 3 [ask patch random-pxcor random-pycor [set-goal]]
end


to set-obstacle
  set obstacle 1
  set pcolor red
  set potential (- potential-level)
end

to set-goal
  set goal 1
   set pcolor yellow
   set potential potential-level
end

to erase-obstacle
  set pcolor black set obstacle 0
  set potential 0
end

to erase-potential
  ask patches [
    ifelse obstacle = 1
    [set-obstacle]
    [ifelse (goal = 1)
        [set-goal]
        [set pcolor black set potential 0]
    ]
  ]
  ask turtles [die]
end
 
to create-agents
  create-turtles number-turtles [
    set color orange
    set size 1.5
    setxy random-xcor random-ycor
    set my-goal max-one-of patches with [goal = 1 ] [distance myself]
    set bloque 0
    set ctask "go-goal"
    
  ]
end


to compute-potential-atract
  set continue true
  while [continue][
    set continue false
    compute-potential1
    tick
  ]
 
end
to compute-potential-repuls
 set continue true
  while [continue]
  [
    set continue false
    compute-potential2
    tick
  ]

end
 
  to compute-potential1
   ask patches 
   [
    ;; code à écrire...
     if obstacle = 0 and goal = 0
     [
       let pn max-one-of neighbors  [potential]
       let pn-neg min-one-of neighbors [potential]
       if pn != nobody
       [
         
         let pn-potential ([potential] of pn) - 1
         if potential < pn-potential 
         [
           set potential pn-potential
           ifelse potential < 0
           [set potential 0]
           [set continue true]  
           set pcolor scale-color green potential 0 (potential-level * 1.2)
         ] 
       ]    
     ]
     
   ] 
       
end
  
to compute-potential2
  ask patches
  [
    if (goal = 0 and obstacle = 0)
    [
      let pn-neg min-one-of neighbors [potential]
      
      let pn-potential  [potential]  of pn-neg + 1
      
      if pn-potential < 0 and potential > pn-potential 
      [
        set potential pn-potential
        ifelse potential >= 0
        [set potential 0 ]
        [set continue true]
        set plabel potential
        ;set pcolor scale-color red abs potential (potential-level * 1.2) 0
      ]
    ]
  ]
end

;;------------------------------------
;;
;;    Turtles
;; 
;;-------------------------------------

to go-turtles
  ask turtles 
  [
    if (goal != 1)
    [
      run ctask
    ]
    
  ]
  
  tick
end

to step-turtles
  ask turtles [go-goal]
end


to go-goal
  let vecteur-dir (list 0 0)
 
  
  if goal != 1 [
    set heading towards my-goal 
    fd 1
  ]
  
  let obstacle-devant min-one-of patches with [obstacle = 1] [distance myself]
  if ( distance obstacle-devant < 2)
  [
    set ctask "go-random"
;    rt random 180 
;    fd 1
    ]
  
  if ( distance my-goal < 1 )
  [
    set goal 1
    ]
  
end

to go-random
  
  set heading random 359 
  if (patch-ahead 1 != nobody and [obstacle] of (patch-ahead 1) = 1)
  [
    set heading (towards (patch-ahead 1) + 180)
    ]
  fd 1
  
  if (ticks mod nb-random-ticks = 0)
  [
    set ctask "go-goal"
    ]
  
  
end

;;----------------------------------------
;;
;;   drawing
;;
;;----------------------------------------

to draw-goals
  if mouse-down?
  [ ask patches
    [ if ((abs (pxcor - mouse-xcor)) < 1) and ((abs (pycor - mouse-ycor)) < 1)
      [ set-goal]]]
  display
end



to draw-obstacles
  if mouse-down?
  [ ask patches
    [ if ((abs (pxcor - mouse-xcor)) < 1) and ((abs (pycor - mouse-ycor)) < 1)
      [ set-obstacle]]]
  display
end

to eraser
  if mouse-down?
  [ ask patches
    [ if ((abs (pxcor - mouse-xcor)) < 1) and ((abs (pycor - mouse-ycor)) < 1)
      [ erase-obstacle]]]
  display
end









to flock  ;; turtle procedure
    find-flockmates
    if any? flockmates
    [let a angleFromVect vectDirect
    turn-towards a max-angle-turn]


;  if any? flockmates
;    [ find-nearest-neighbor
;      ifelse distance nearest-neighbor < minimum-separation
;;        [ separate ]
;;
;;        [ align
;;          cohere ] ]
end


to-report angleFromVect [vect]
    let a atan item 0  vect item 1 vect
    report a
end

to-report vectDirect


let ts other turtles in-cone dist-vision vision
let ps patches in-cone dist-vision vision with [potential < 0 ]

let force-attrac vectFromAngle heading 1

let p-obstacle min-one-of ps [potential]

let nearest-t min-one-of ts [distance myself]

if (nearest-t != nobody)
[
  let dist-nearest-t distance nearest-t
  
]

if (p-obstacle != nobody )
[
  let dist-nearest-p-obst distance p-obstacle
  let force-evit vectFromAngle (p-obstacle + 90) 1 / dist-nearest-p-obst
  let force-repuls vectFromAngle (towards p-obstacle + 180) 1 / dist-nearest-p-obst
  let vect-dir additionvect force-repuls force-evit
  set vect-dir additionvect vect-dir force-attrac

  report vect-dir
]





end

to find-flockmates  ;; turtle procedure
  set flockmates other turtles with  [color = [color] of myself] in-radius vision
end

to find-nearest-neighbor ;; turtle procedure
  set nearest-neighbor min-one-of flockmates [distance myself]
end

;;; SEPARATE


to-report vectSeparate
  let vs 0
  find-nearest-neighbor
  ifelse (nearest-neighbor = nobody)
  [set vs VectFromAngle random 180 0]
  [set vs VectFromAngle (towards nearest-neighbor + 180 ) (1 / distance nearest-neighbor)]
  report vs
end

;to separate  ;; turtle procedure
;  turn-away ([heading] of nearest-neighbor) max-separate-turn
;end

;;; ALIGN

to-report vectAlign
  let x-component sum [dx] of flockmates
  let y-component sum [dy] of flockmates
  report (list x-component y-component)
end

;to align  ;; turtle procedure
;  turn-towards average-flockmate-heading max-align-turn
;end

;to-report average-flockmate-heading  ;; turtle procedure
;  ;; We can't just average the heading variables here.
;  ;; For example, the average of 1 and 359 should be 0,
;  ;; not 180.  So we have to use trigonometry.
;  let x-component sum [dx] of flockmates
;  let y-component sum [dy] of flockmates
;  ifelse x-component = 0 and y-component = 0
;    [ report heading ]
;    [ report atan x-component y-component ]
;end

;;; COHERE

;to cohere  ;; turtle procedure
;  turn-towards average-heading-towards-flockmates max-cohere-turn
;end

to-report vectCohere

  let x-component mean [sin (towards myself + 180)] of flockmates
  let y-component mean [cos (towards myself + 180)] of flockmates
  report (list x-component y-component)
end


;to-report average-heading-towards-flockmates  ;; turtle procedure
;  ;; "towards myself" gives us the heading from the other turtle
;  ;; to me, but we want the heading from me to the other turtle,
;  ;; so we add 180
;
;  let x-component mean [sin (towards myself + 180)] of flockmates
;  let y-component mean [cos (towards myself + 180)] of flockmates
;  ifelse x-component = 0 and y-component = 0
;    [ report heading ]
;    [ report atan x-component y-component ]
;end

;;; HELPER PROCEDURES

to turn-towards [new-heading max-turn]  ;; turtle procedure
  turn-at-most (subtract-headings new-heading heading) max-turn
end

to turn-away [new-heading max-turn]  ;; turtle procedure
  turn-at-most (subtract-headings heading new-heading) max-turn
end

;; turn right by "turn" degrees (or left if "turn" is negative),
;; but never turn more than "max-turn" degrees
to turn-at-most [turn max-turn]  ;; turtle procedure
  ifelse abs turn > max-turn
    [ ifelse turn > 0
        [ rt max-turn ]
        [ lt max-turn ] ]
    [ rt turn ]
end

to-report multiplyScalarvect [factor vect]
   report (list (item 0 vect * factor) (item 1 vect * factor))
end
to-report additionvect [v1 v2]
   report (list (item 0 v1 + item 0 v2) (item 1 v1 + item 1 v2) )
end
to-report vectFromAngle [angle len]
   let l (list (len * sin angle) (len * cos angle))
   report l
end
@#$#@#$#@
GRAPHICS-WINDOW
234
10
654
451
20
20
10.0
1
10
1
1
1
0
0
0
1
-20
20
-20
20
0
0
1
ticks
30.0

BUTTON
10
20
106
53
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
5
189
100
222
NIL
go-turtles
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
4
309
113
342
Draw Obstacles
draw-obstacles
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
5
356
115
389
Erase obstacles
eraser
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
254
529
433
562
potential-field-atract
compute-potential-atract\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
4
400
217
433
potential-level
potential-level
0
100
0
1
1
NIL
HORIZONTAL

BUTTON
446
525
621
558
step potential atrac
\ncompute-potential1\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
112
12
209
45
Erase potential
erase-potential
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
4
145
176
178
number-turtles
number-turtles
0
200
42
1
1
NIL
HORIZONTAL

BUTTON
108
188
213
221
NIL
step-turtles
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
123
310
223
343
Draw goals
draw-goals
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
7
102
106
135
Create agents
create-agents
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

CHOOSER
8
239
146
284
diffusion-method
diffusion-method
"diffuse" "flooding"
0

SLIDER
5
438
217
471
diffusion-rate
diffusion-rate
0
100
74
1
1
NIL
HORIZONTAL

SLIDER
4
477
220
510
diffusion-turn
diffusion-turn
0
100
86
1
1
NIL
HORIZONTAL

BUTTON
239
465
441
498
potential field repulsive
compute-potential-repuls
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
461
471
643
504
step potential repuls
compute-potential2
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
713
201
885
234
vision
vision
0
100
50
1
1
NIL
HORIZONTAL

SLIDER
711
286
914
319
minimum-separation
minimum-separation
0
5
0.5
0.1
1
NIL
HORIZONTAL

SLIDER
713
358
885
391
max-angle-turn
max-angle-turn
0
180
51
1
1
NIL
HORIZONTAL

SLIDER
726
250
898
283
dist-vision
dist-vision
0
100
50
1
1
NIL
HORIZONTAL

SLIDER
23
548
196
581
nb-random-ticks
nb-random-ticks
0
100
38
1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

A flooding algorithm for creating potential fields with obstacles

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)
The 'step potential' button

## HOW TO USE IT

draw goals and then use '

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)
Draw obstacles that allow for only a part
Change the value of the potential field. See the behavior of agents when there is no field (it's black). They do not go towards the goals.

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(C) J. Ferber 2014 
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
0
Rectangle -7500403 true true 151 225 180 285
Rectangle -7500403 true true 47 225 75 285
Rectangle -7500403 true true 15 75 210 225
Circle -7500403 true true 135 75 150
Circle -16777216 true false 165 76 116

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -7500403 true true 135 285 195 285 270 90 30 90 105 285
Polygon -7500403 true true 270 90 225 15 180 90
Polygon -7500403 true true 30 90 75 15 120 90
Circle -1 true false 183 138 24
Circle -1 true false 93 138 24

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
