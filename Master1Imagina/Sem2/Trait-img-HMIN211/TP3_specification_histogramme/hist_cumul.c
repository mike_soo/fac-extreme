// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#include "lib.h"

int main(int argc, char* argv[])
{
 
  
  char cNomImgLue[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 2)
  {
    printf("Rentrez imageLue\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  

  OCTET *ImgIn, * ImgOut;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig, &nb_col);
  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille);
  

  lire_image_pgm(cNomImgLue, ImgIn, nb_col * nb_lig);
  unsigned int H[256];
  unsigned long long int HC[256];

  hist(ImgIn , H , nb_lig , nb_col);
  // printf("H\n");
  // for (int i = 0 ; i< 256 ; i++)
  // {
  //   printf("%i %i\n",i,H[i]);
  // }


  hist_cumule(ImgIn , H , HC , nb_lig , nb_col);
  
  printf("Intensité \"Hist Cumulé\"\n");
  for (int i = 0 ; i< 256 ; i++)
  {
    printf("%i %llu\n",i,HC[i] / nTaille);
  }

  free(ImgIn);
  return 1;
}
