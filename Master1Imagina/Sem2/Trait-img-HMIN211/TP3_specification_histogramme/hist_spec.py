#!/usr/bin/env python3
import subprocess
import sys

if __name__ == "__main__":
	img_name = sys.argv[1]
	img_no_ext_name = img_name[:-4]
	img_spec = sys.argv[2]
	img_spec_no_ext = img_spec[:-4]
	img_out_name = img_no_ext_name + "_spec_" + img_spec_no_ext  + ".pgm"
	ext_data = ".dat"
	plot_plg = "plot-hist-spec.plg"
	c_prog = "./specialisation"
	hist_dat_name = img_no_ext_name + "_hist_spec" + ext_data
	
	cmd1=[]
	cmd1.append(c_prog)
	cmd1.append(img_name)
	cmd1.append(img_spec)
	cmd1.append(img_out_name)
	with open(hist_dat_name, "w") as outfile:
		subprocess.call(cmd1, stdout=outfile)
	
	cmd2 =[]
	cmd2.append("gnuplot")
	cmd2.append("-e")
	cmd2.append("filename=\""+ hist_dat_name +"\"")
	cmd2.append(plot_plg)
	subprocess.call(cmd2)
	