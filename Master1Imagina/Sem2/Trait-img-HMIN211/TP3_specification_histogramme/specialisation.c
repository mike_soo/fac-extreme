// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#include "lib.h"

int main(int argc, char* argv[])
{
 
  
  char cNomImgLue[250] , cNomImgSpec[250] , cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 4)
  {
    printf("Rentrez imageLue imageSpec imageEcr\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s" , cNomImgLue) ;
  sscanf (argv[2], "%s" , cNomImgSpec);
  sscanf (argv[3], "%s" , cNomImgEcrite) ;

  OCTET *ImgIn , *ImgOut , *ImgSpec;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig, &nb_col);
  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille);
  allocation_tableau(ImgOut, OCTET, nTaille);
  allocation_tableau(ImgSpec, OCTET, nTaille);

  lire_image_pgm(cNomImgLue, ImgIn, nb_col * nb_lig);
  lire_image_pgm(cNomImgSpec , ImgSpec , nb_col * nb_lig);

  struct spec_elems se;
  unsigned int tab_hist_im_in[256];
  unsigned int tab_hist_im_spec[256];

  unsigned long long int tab_hist_cumule_im_in [256];
  unsigned long long int tab_hist_cumule_im_spec[256];

  unsigned int tab_egalisation_im_in[256];
  unsigned int tab_egalisation_im_spec[256];

  unsigned int map[256];

  se.tab_hist_im_in=tab_hist_im_in;
  se.tab_hist_im_spec=tab_hist_im_spec;

  se.tab_hist_cumule_im_in =tab_hist_cumule_im_in ;
  se.tab_hist_cumule_im_spec=tab_hist_cumule_im_spec;

  se.tab_egalisation_im_in=tab_egalisation_im_in;
  se.tab_egalisation_im_spec=tab_egalisation_im_spec;

  se.map=map;



  specification(ImgIn , ImgSpec , ImgOut , nb_lig , nb_col , &se);
  
  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nb_col, nb_lig);


  free(ImgIn);
  return 1;
}
