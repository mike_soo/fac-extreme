// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
 
  
  char cNomImgLue[250], cNomImgCarteVerite[250];
  int nb_col, nb_lig, nTaille;
  

  if (argc != 3)
  {
    printf("Rentrez imageLue imageCarteVerite\n");
    exit (-1) ;
  }
  sscanf (argv[1], "%s", cNomImgLue) ;
  sscanf (argv[2], "%s", cNomImgCarteVerite) ;
  
  
  OCTET *ImgIn, * ImgCarteVerite , *ImgSeuillee;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig, &nb_col);

  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille);
  allocation_tableau(ImgCarteVerite, OCTET, nTaille);
  allocation_tableau(ImgSeuillee, OCTET, nTaille);

  lire_image_pgm(cNomImgLue, ImgIn, nb_col * nb_lig);
  lire_image_pgm(cNomImgCarteVerite, ImgCarteVerite, nb_col * nb_lig);
  
  printf("# specificitée sensibilitee\n");
  double min = 255 , min_temp;
  for(int seuil = 0 ; seuil < 256 ; seuil ++ )
  {
    // printf("\n\nseuil %i\n", seuil);
    for (int i = 0 ; i < nTaille ; i ++)
    {
      
      if(ImgIn[i]> seuil)
        ImgSeuillee[i] = 255;
      else
        ImgSeuillee[i] = 0;
    }


    
    unsigned long int TN = 0 , TP = 0 , FN = 0 , FP = 0;
    for(int i =0 ; i< nTaille ; i++)
    {
      if (ImgSeuillee[i] == ImgCarteVerite[i] && ImgCarteVerite[i]== 255)
      {
        TP++;
      }
      else if(ImgSeuillee[i] == ImgCarteVerite[i] && ImgCarteVerite[i]== 0)
      {
        TN++;
      }
      else if(ImgSeuillee[i] != ImgCarteVerite[i] && ImgSeuillee[i]==0)
      {
        //ImgSeuille[i] == 0 => il est du fond alors que non egale à imgCarteVerite[i]
        FN++;
      }
      else if(ImgSeuillee[i] != ImgCarteVerite[i] && ImgSeuillee[i]==255)
      {
        FP++;
      }
    }
    // printf("TP=%li TN=%li FP=%li FN=%li\n" , TP , TN , FP ,FN);
    double specificitee;
    double sensibilitee;

    if(TN == 0)
    {
      specificitee = 0.0;
    }
    else
    {
      specificitee  =(double) TN / (TN + FP);
    }
    if (TP == 0)
    {
      sensibilitee = 0.0 ;
    }
    else
    {
      sensibilitee = (double) TP / (TP + FN);
    }
    min_temp= (1.0 - sqrt(pow(sensibilitee, 2.0) + pow((1.0 - specificitee),2.0)));
    printf("min_temp %lf\n" , min_temp);
    if( min_temp< min)
    {
      min = min_temp; 
      printf("\tmin def %i\n\n", seuil);
    }
    // double rappel = (double) TP / (TP + FN);
    // printf("%lf %lf\n" , 1 - specificitee , sensibilitee);
  }
  

  // printf("seuil opti = %i" , min);
  



  return 1;
}
