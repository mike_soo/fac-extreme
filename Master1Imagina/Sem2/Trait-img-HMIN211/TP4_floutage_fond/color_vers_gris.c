// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  
  
  char cNomImgLue[250], cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;

  if (argc != 3)
  {
    printf("Rentrez imageLue imageEcr\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  sscanf (argv[2], "%s", cNomImgEcrite) ;
  
  OCTET *ImgIn, * ImgOut;
  
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nb_lig, &nb_col);
  printf("nombre de ligne : %i\n",nb_col);
  printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille*3);
  allocation_tableau(ImgOut, OCTET, nTaille);
  
  lire_image_ppm(cNomImgLue, ImgIn, nTaille);
  

  OCTET val_gris;
  int j = 0;
  for (int i = 0 ; i < nTaille * 3 ; i +=3)
  {
    
    val_gris = (ImgIn[i] *0.3 + ImgIn[i+1] *0.59 + ImgIn[i+2] *0.11);
    ImgOut[j] = val_gris;
    j++;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut , nb_lig ,nb_col);

  
  
  free(ImgIn);
  return 1;
}
