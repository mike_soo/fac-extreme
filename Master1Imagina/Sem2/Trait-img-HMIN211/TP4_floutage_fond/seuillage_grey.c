// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
 
  
  char cNomImgLue[250], cNomImgEcrite[250];
  int nb_col, nb_lig, nTaille;
  int seuil;

  if (argc != 4)
  {
    printf("Rentrez imageLue imageEcr seuil\n");
    exit (-1) ;
  }
  sscanf (argv[1], "%s", cNomImgLue) ;
  sscanf (argv[2], "%s", cNomImgEcrite) ;
  sscanf (argv[3], "%d", &seuil);
  
  OCTET *ImgIn, * ImgOut;

  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig, &nb_col);
  // printf("nombre de ligne : %i\n",nb_col);
  // printf("nombre de colone : %i\n",nb_lig);
  nTaille = nb_col * nb_lig;
  // printf("nTaille = %i\n", nTaille);
  allocation_tableau(ImgIn, OCTET, nTaille);
  allocation_tableau(ImgOut, OCTET, nTaille);

  lire_image_pgm(cNomImgLue, ImgIn, nb_col * nb_lig);
  

  printf("seuil %i \n" , seuil );
  for (int i = 0 ; i < nTaille ; i ++)
  {
    if(ImgIn[i]> seuil)
      ImgOut[i] = 255;
    else
      ImgOut[i] = 0;
  }
  ecrire_image_pgm(cNomImgEcrite, ImgOut , nb_lig , nb_col);

  free(ImgIn);
  free(ImgOut);
  return 1;
}
