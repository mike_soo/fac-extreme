// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  char cNomImgLue[250], choix_lig_col;
  int nH, nW, nTaille, S1, S2, S3, x;

  if (argc != 4)
  {
    printf("Usage: ImageIn.pgm [c | l] x \n où x est un nombre compris entre 0 511");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  sscanf (argv[2], "%c", &choix_lig_col);
  sscanf (argv[3], "%d", &x);

  OCTET *ImgIn;
  if(x < 0 || x > 511)
  {
    printf("x doit être comprit entre 0 et 511\n");
    exit(-1);
  }
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  // printf("nombre de ligne : %i\n",nH);
  // printf("nombre de colone : %i\n",nW);
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  unsigned char val;
  
  if (choix_lig_col == 'c')
  {
    for (long i = 0 ; i < nH ; i++)
    {
      val = (unsigned char) ImgIn[(i * nW) + x];
      printf("%lu %u\n",i,val);
    }
  }

  else if (choix_lig_col == 'l')
  {
    long ind = 0; 
    for (long i = x*nH ; i <  (x*nH) + nW  ; i++)
    {
      val = (unsigned char) ImgIn[i];
      printf("%lu %u\n",ind,val);
      ind++;
    }
  }

  free(ImgIn);
  return 1;
}
