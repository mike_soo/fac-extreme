// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  char cNomImgLue[250], choix_lig_col;
  int nH, nW, nTaille;

  if (argc != 2)
  {
    printf("Rentrez image en param\n");
    exit (-1) ;
  }

  sscanf (argv[1], "%s", cNomImgLue) ;
  
  OCTET *ImgIn;
  
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  // printf("nombre de ligne : %i\n",nH);
  // printf("nombre de colone : %i\n",nW);
  allocation_tableau(ImgIn, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
  unsigned char H[256];

  for (unsigned int i = 0 ; i < nTaille ; i ++)
  {
    H[ImgIn[i]]++;
  }

  for (unsigned int i = 0 ; i < 256 ; i ++)
  {
    printf("%u %u\n", i , H[i]);
  }



  free(ImgIn);
  return 1;
}
