#!/bin/bash

# if [ ! -f $1 ]; then
# 	echo "File $1 not found"
# 	exit
# fi

# if [ "$2" != "l" -a "$2" != "c" ]; then
# 	echo "$2 no reconnue : Rentrez l ou c "
# 	exit
# fi

# if [ $3 -lt "0" -o $3 -ge "511" ]; then
# 	echo "Entier comprit entre 0 et 511"
# 	exit
# fi
dat_name="$1_histo_coul.dat"
./histogramme-coul $1 > $dat_name
gnuplot -e "filename='$dat_name'" plot-hist-coul.plg