#include "lib.h"
#include "image_ppm.h"

int main(int argc, char* argv[])
{
	char cNomImgLue[250], cNomImgEcrite[250], cNomImgDilat[250];
	int nb_lig_im, nb_col_im, nTaille;
	int nb_lig_im_dil, nb_col_im_dil, nTaille_dil; 

	if (argc != 4)
	{
		printf("Usage: ImageIn.pgm ImgDilat.pgm ImageOut.pgm Seuil \n");
		exit (1) ;
	}

	sscanf (argv[1], "%s", cNomImgLue) ;
	sscanf (argv[2], "%s", cNomImgDilat) ;
	sscanf (argv[3], "%s", cNomImgEcrite);


	OCTET *ImgIn1D, **ImgIn2D,*ImgInDil1D, **ImgInDil2D, *ImgOut1D , **ImgOut2D;


	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig_im, &nb_col_im);
	nTaille = nb_lig_im * nb_col_im;

	lire_nb_lignes_colonnes_image_pgm(cNomImgDilat , &nb_lig_im_dil , &nb_col_im_dil);
	nTaille_dil = nb_lig_im_dil * nb_col_im_dil;

	allocation_tableau(ImgIn1D, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn1D, nb_lig_im * nb_col_im);
	allocation_tableau(ImgOut1D, OCTET, nTaille);
	
	allocation_tableau(ImgInDil1D, OCTET, nTaille_dil);
	lire_image_pgm(cNomImgDilat, ImgInDil1D, nb_lig_im_dil * nb_col_im_dil);
	allocation_tableau(ImgOut1D, OCTET, nTaille_dil);
	


	ImgIn2D = allocation_tableau2D(nb_lig_im,nb_col_im);
	ImgInDil2D = allocation_tableau2D(nb_lig_im_dil,nb_col_im_dil);
	ImgOut2D = allocation_tableau2D(nb_lig_im,nb_col_im);

	conv1Dvers2D( ImgIn1D , ImgIn2D , nb_lig_im , nb_col_im);
	conv1Dvers2D( ImgInDil1D , ImgInDil2D , nb_lig_im_dil ,nb_col_im_dil);

	diference( ImgIn2D, ImgInDil2D , ImgOut2D , nb_lig_im , nb_col_im );

	conv2Dvers1D( ImgOut1D, ImgOut2D, nb_lig_im ,nb_col_im);

	ecrire_image_pgm(cNomImgEcrite, ImgOut1D,  nb_lig_im, nb_col_im);
	free(ImgIn1D);
	exit( 0);
}
