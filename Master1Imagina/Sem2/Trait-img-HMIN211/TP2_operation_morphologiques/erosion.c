#include "lib.h"
#include "image_ppm.h"
int main(int argc, char* argv[])
{
	char cNomImgLue[250], cNomImgEcrite[250];
	int nb_lig_im, nb_col_im, nTaille;

	if (argc != 3)
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm \n");
		exit (1) ;
	}

	sscanf (argv[1], "%s", cNomImgLue) ;
	sscanf (argv[2], "%s", cNomImgEcrite);


	OCTET *ImgIn1D, **ImgIn2D, *ImgOut1D , **ImgOut2D;


	lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nb_lig_im, &nb_col_im);
	nTaille = nb_lig_im * nb_col_im;

	allocation_tableau(ImgIn1D, OCTET, nTaille);
	lire_image_pgm(cNomImgLue, ImgIn1D, nb_lig_im * nb_col_im);
	allocation_tableau(ImgOut1D, OCTET, nTaille);
	
	ImgIn2D = allocation_tableau2D(nb_lig_im,nb_col_im);
	ImgOut2D = allocation_tableau2D(nb_lig_im,nb_col_im);

	conv1Dvers2D( ImgIn1D , ImgIn2D , nb_lig_im , nb_col_im);

	erosion(ImgIn2D, ImgOut2D , nb_lig_im , nb_col_im );

	conv2Dvers1D( ImgOut1D, ImgOut2D, nb_lig_im ,nb_col_im);

	ecrire_image_pgm(cNomImgEcrite, ImgOut1D,  nb_lig_im, nb_col_im);
	free(ImgIn1D);
	exit(0);
}
