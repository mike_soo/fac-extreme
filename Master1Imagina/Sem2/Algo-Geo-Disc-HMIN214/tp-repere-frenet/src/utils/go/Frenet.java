package utils.go;

import java.util.ArrayList;

public class Frenet {
	
	public static Repere genRepere(String f,double t, PointVisible origine , double omega)
	{
		Vecteur vtan = null , vnorm = null;
		PointVisible tanTo , normTo , from = new PointVisible(origine.getXrounded() + t, origine.getYrounded() + (10*Math.sin(omega*t)));
		from.setLabel("from_sin");
		double a,b;
		if(f.equals("sin"))
		{	
			a =  100* (1 / (Math.sqrt(1  +  ( Math.pow(10*omega * Math.cos(omega*t), 2)) )));
			b =  100*10* omega * Math.cos(omega * t) / (Math.sqrt(1 + Math.pow(10*omega * Math.cos(omega*t), 2)));
			
			System.out.println("a et b " + a+" " + b );
			
			tanTo  = new PointVisible(a , b );
			vtan   = new Vecteur(from , tanTo);
			normTo = new PointVisible(-b , a );
			vnorm  = new Vecteur(from , normTo) ;
			
			tanTo.setLabel("tanTo_sin");
			normTo.setLabel("normTo_sin");
			
			
			System.out.println("repere de frenet init");
			System.out.println("Vue.Init > vtan From x: "+ vtan);
			System.out.println("Vue.Init > vnorm To x: "+ vnorm);
		}
		else 
		{
			System.out.println("Frenel : tan() warning retour de null");
			return null;
		}
		ArrayList<Vecteur> repereList = new ArrayList<Vecteur> ();
		repereList.add(vtan);
		repereList.add(vnorm);
		
		
		return  new Repere(from , repereList);
	}
}
