package utils.aff;

import java.awt.Graphics2D;

import utils.go.PointVisible;
import utils.go.Vecteur;

public class Courbes {
	
	public static void drawF (Graphics2D g , Vecteur droiteTrajectoire , double omega ,long N , String f)
	{
		
		int distanceAParcourir = (int) Math.sqrt(droiteTrajectoire.norme1());
//		System.out.println("drawSin , droiteTrajectoire : " +
//				droiteTrajectoire + " " + " distance à parcourir : "+ distanceAParcourir);
		PointVisible pcour = new PointVisible(0.1 , 0.1), 
				     psuiv = new PointVisible(0.1 , 0.1);
		pcour.setLabel("pcour");
		psuiv.setLabel("psuiv");
		double theta, xcour, xsuiv, ycour = 0.0 , ysuiv = 0.0, dx=(double)distanceAParcourir/N;
//		System.out.println("dx = "+dx);
			for(long i = 0 ; i < N ; i ++)
			{
				
				xcour = (double)i * (double)dx;
				xsuiv = (double)(i+ 1) * (double)dx;
				
				if(f == "sin")
				{
					ycour = 10*Math.sin(xcour * omega);
					ysuiv = 10*Math.sin(xsuiv * omega);
				}
				
				if(f =="cos")
				{
					ycour = 10*Math.cos(xcour * omega);
					ysuiv = 10*Math.cos(xsuiv * omega);
				}
				if(f == "puis2")
				{
					ycour = Math.pow(xcour,2);
				    ysuiv = Math.pow(xsuiv, 2);
				}
//				System.out.println("xcour = " + xcour);
//				System.out.println("cour = " + ycour);
//				System.out.println("xsuiv = " + xsuiv);
//				System.out.println("suiv = " + ysuiv);

				pcour.getMC().x =  
					droiteTrajectoire.getFrom()
						.getMC().x + xcour;
				pcour.getMC().y = 	
					droiteTrajectoire.getFrom()
						.getMC().y + ycour;
				
				psuiv.getMC().x = 
					droiteTrajectoire.getFrom()
						.getMC().x + xsuiv;
				psuiv.getMC().y = 
					droiteTrajectoire.getFrom()
						.getMC().y + ysuiv;
				
				g.drawLine(pcour.getXrounded(), pcour.getYrounded(), 
						   psuiv.getXrounded(), psuiv.getYrounded());	
				
				
				
//				System.out.println(" " + pcour);
//				System.out.println(" " + psuiv);
			}
	}
}
