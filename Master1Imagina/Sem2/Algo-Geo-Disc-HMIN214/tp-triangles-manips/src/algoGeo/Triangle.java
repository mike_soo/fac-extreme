package algoGeo;

import java.awt.Graphics2D;
import java.util.ArrayList;

import affichage.VisiblePoint;

public class Triangle {
	VisiblePoint sommets[];
	VisiblePoint sommetG=null;
	String label;
	
	private ArrayList<Triangle> trianglesDeG ;
	
	private ArrayList<Triangle> trianglesDeM ;
	
	public Triangle() {
		sommets  = new VisiblePoint[3];
		//setCentreDeGravite();
		trianglesDeG= new ArrayList<Triangle>();
		trianglesDeM= new ArrayList<Triangle>();
	}

	public Triangle(VisiblePoint p1, VisiblePoint p2, VisiblePoint p3, String l) {
		sommets = new VisiblePoint[3];
		sommets[0] = p1;
		sommets[1] = p2;
		sommets[2] = p3;
		label = l;
		//setCentreDeGravite();
		trianglesDeG= new ArrayList<Triangle>();
	}
	
	public void add(VisiblePoint p) {
		int j= 0;
		for (int i = 0; i<sommets.length; i++)
			if (sommets[i]!= null) j++;
		if (j< 3) sommets[j] = p;
	}
	
	public void dessine(Graphics2D g) {
		g.drawLine((int) sommets[0].x , (int) sommets[0].y , (int) sommets[1].x, (int) sommets[1].y);
		g.drawLine((int) sommets[0].x , (int) sommets[0].y , (int) sommets[2].x, (int) sommets[2].y);
		g.drawLine((int) sommets[2].x , (int) sommets[2].y , (int) sommets[1].x, (int) sommets[1].y);
		
		
		for (int i = 0; i<sommets.length; i++)
			sommets[i].dessine(g);
		
		//Affiche centre de gravité
		getCentreDeGravite().dessine(g);
		System.out.println("G" + sommetG.toString());
		
		for (int i = 0 ; i< trianglesDeG.size(); i ++){
			trianglesDeG.get(i).dessineFils(g);
		}
	}
	
	public void dessineFils(Graphics2D g)
	{
		g.drawLine((int) sommets[0].x , (int) sommets[0].y , (int) sommets[1].x, (int) sommets[1].y);
		g.drawLine((int) sommets[0].x , (int) sommets[0].y , (int) sommets[2].x, (int) sommets[2].y);
		g.drawLine((int) sommets[2].x , (int) sommets[2].y , (int) sommets[1].x, (int) sommets[1].y);
		
	}
	
	public VisiblePoint getCentreDeGravite()
	{
		if (sommetG == null)
		{
			setCentreDeGravite();
		}
		return this.sommetG;
	}
	
	public void setCentreDeGravite()
	{
		double xG;
		double yG;
		
		
		xG= (1/3) * (sommets[0].getX() + sommets[1].getX() + sommets[2].getX());  
		yG= (1/3) * (sommets[0].getY() + sommets[1].getY() + sommets[2].getY());
		
		this.sommetG = new VisiblePoint(xG,yG);
	}
	
	public double calcAire()
	{
		return Math.abs((1/2) * sommets[0].det(sommets[1])); 
	}
	
	public void setTrianglesDeG()
	{
		trianglesDeG.add(new Triangle(sommets[0],sommetG,sommets[1],"triangleDeG1"));
		trianglesDeG.add(new Triangle(sommets[1],sommetG,sommets[2],"triangleDeG2"));
		trianglesDeG.add(new Triangle(sommets[2],sommetG,sommets[0],"triangleDeG2"));
	}
	
	public double calcAireTriangleCentreGravite()
	{
		if (trianglesDeG.size()>0)
		{
			return trianglesDeG.get(0).calcAire();
		}
		return 0;
	}
	public boolean airesTrianglesCentreGraviteEgaux()
	{
		double aire=0;
		
		for (int i = 0 ; i < trianglesDeG.size() ; i++)
		{
			if (i==0)
			{
				aire = trianglesDeG.get(i).calcAire();
			}
			else
			{
				if (trianglesDeG.get(i).calcAire() != aire )
					return false;
			}
		}
		return true;
	}
	
	//Incomplet
	public double setTrianglesDeMedianes()
	{
		int ind = 0;
		for (int i = 0 ; i < 3 ; i++)
		{
			 
			trianglesDeM.add(new Triangle(sommets[i],sommetG,sommets[i].I(sommets[(i + 1) % 3 ]) ,"TdeM" + i ));
			trianglesDeM.add(new Triangle (sommets[ind].I(sommets[(ind + 1) % 3 ]),sommetG,sommets[(ind + 1) % 3] , "TdeM" + i));
		}
		return 0;
	}
}

