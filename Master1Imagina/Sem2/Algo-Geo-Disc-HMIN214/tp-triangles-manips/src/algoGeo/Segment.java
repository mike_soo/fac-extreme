package algoGeo;

import java.awt.Graphics2D;
import java.lang.reflect.Array;
import java.util.ArrayList;

import affichage.VisiblePoint;

public class Segment {
	VisiblePoint A;
	VisiblePoint B;	
	boolean testIntersection (Segment S) throws Exception
	{
		
		int d1 = A.tour(this.B , S.getC());
		int d2 = A.tour(this.B, S.getD());
		
		if (d1 * d2 > 0)
		{
			return false;
		}
		if(d1 * d2 < 0)
		{
			int d3 = S.getC().tour(S.getD(), A);
			int d4 = S.getC().tour(S.getD(), B);
			if(d3 * d4 > 0 )
			{
				return false;
			}
			if(d3 * d4 < 0 )
			{
				return true;
			}
			
			
		}
		
		
		//d1 * d2 == 0
		return S.contient(A) || S.contient(B) || S.contient(S.getC()) || S.contient(S.getD());
	}
	
	private boolean contient(VisiblePoint P) throws Exception {
		//cas vertical
		if ((A.getX() == B.getX()) && (A.getY() == B.getY()))
		{
			return (A.getX() ==  P.getX() && A.getY() == P.getY());
		}
		
		
		//cas vertical
		if ((A.getY() - B.getY()) != 0 && A.getX() == B.getX())
		{
			return (P.getX() == A.getX()) 
					&& (P.getY() - B.getY()) / (A.getY() - B.getY()) >= 0 
					&& (P.getY() - B.getY()) / (A.getY() - B.getY()) <= 1; 
					
		}
		
		//cas horizontal
		if ((A.getX() - B.getX()) != 0  && A.getY() == B.getY())
		{
			return (P.getY() == A.getY()) 
					&& (P.getX() - B.getX()) / (A.getX() - B.getX()) >= 0 
					&& (P.getX() - B.getX()) / (A.getX() - B.getX()) <= 1; 
					
		}
		
		//sinon
		if ((A.getX() - B.getX()) != 0 && (A.getY() - B.getY()) != 0)
		{
			System.out.println("Division par zero");
			throw new Exception ();
		}
		else
		{
			return  (P.getX() - B.getX()) / (A.getX() - B.getX()) == (P.getY() - B.getY()) / (A.getY() - B.getY()) 
					&& (P.getX() - B.getX()) / (A.getX() - B.getX()) >= 0 
					&& (P.getX() - B.getX()) / (A.getX() - B.getX()) <= 1;
		}
	}
	
	
	ArrayList <VisiblePoint> intersections (Segment s)
	{
		
		double[][]  tabM= 
		{
			{A.getX() - B.getX() , s.getD().getX() - s.getC().getX()},
			{A.getY()  - s.getD().getY() ,s.getD().getY() - s.getC().getY() }
		};
		
		
		double[][] tabm = 
		{
				{s.getD().getX() - B.getX()},
				{s.getD().getY() - B.getY()}
		};
	
		Matrice M = new Matrice(tabM);
		Matrice m = new Matrice (tabm);
		
		
				
		if(M.getDeterminant() != 0)
		{
			Matrice T = M.getMatriceInverse().multiply(m);
			if(T.getValue(0, 0) >= 0 && T.getValue(0, 0) <= 1
				&& T.getValue(1, 0) >= 0 && T.getValue(1, 1) <= 1)
			{
				double t = T.getValue(0,0);
				double tp = T.getValue(0,1);
				ArrayList<VisiblePoint> pointsIntersections = new ArrayList<VisiblePoint> ();
				pointsIntersections.add(new VisiblePoint( A.multScal(t).add( B.multScal(1-t))));
				return pointsIntersections;
			}
		}
		return null;
		
	}
	
	public void dessine(Graphics2D g) {
		g.drawLine((int) A.x ,(int) A.y ,(int) B.x, (int) B.y);
		
	}
	
	
	public VisiblePoint getC()
	{
		return this.A;
	}
	
	public VisiblePoint getD()
	{
		return this.B;
	}
}
