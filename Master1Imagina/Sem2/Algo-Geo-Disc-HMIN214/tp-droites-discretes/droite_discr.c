// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#include "lib.h"
#include "lib_droites.h"

const unsigned int SIZE_OF_FILE_BUFFER = 256;
int main(int argc, char* argv[])
{
  char cNomImgEcrite[250];
  int nb_col_img = 128, nb_lig_img=128, nTaille;  
  if (argc != 2)
  {
    printf("Rentrez imageEcr\n");
    exit (-1) ;
  }
  sscanf (argv[1], "%s", cNomImgEcrite) ;

  OCTET * ImgOut;
  OCTET ** ImgOut2d;
  nTaille = nb_col_img * nb_lig_img;
  
  allocation_tableau(ImgOut, OCTET, nTaille);

  ImgOut2d = allocation_tableau2D(nb_lig_img, nb_col_img);
  

  int x0 = 0;
  int y0 = nb_lig_img - 1;

  int x1_start = x0 + 80;
  int x1 = x1_start; 

  int y1_start = y0 - 50;
  int y1 = y1_start;


  for (int i = 0 ; i < nb_lig_img ; i++)  
  {
    for (int j = 0 ; j < nb_col_img ; j++)
    {
      ImgOut2d[i][j] =0;
    }
  }

  /*
    =================== TP =====================
  */

  // 8° Implémentation d'algorithmes 
  // algo_bressen_ham(x0 , y0 , x1 , y1 , ImgOut2d , nb_lig_img , nb_col_img , 180);
  // algo_reveilles(x0  , y0 , x1 , y1 , ImgOut2d , nb_lig_img , nb_col_img , 255);

  int epaisseur = 8;

  // 9° Traçage de droites épaisses
  // On essaye donc de tracer des droites épaisses en dessinant plusieurs droites de manière continue
  // Pour avoir un rendu visuel approprié on décale le point de départ et d'arrivé de chaque droites.
  // Dans les coordonnées mathématiques, on incrémente x et y de chaque point d'arrivé crées pour réussir à modéliser
  // l’épaisseur de la manière suivante:
  // Bresen-Ham

  x1 = x1_start;  
  // for (int i = 0 ; i < epaisseur ; i ++)
  // {
  //   // y0 (mathematique) incrémenté à chaque itération donc à chaque droite successive dessiné s.
  //   y0 = nb_lig_img - 20 - (i);

  //   // y1 (mathematique) incrémenté de 2 unité à chaque itération si i est pair , de 1 sinon
  //   y1 = y0 - 40 - (i%2 == 0? 1 : 0) ;
  //   ++x1;
  //   algo_bressen_ham(x0 , y0 , x1 , y1 , ImgOut2d , nb_lig_img , nb_col_img , 80 + i*(20));  
  // }

  
  
  // Réveilles 
  x1 = x1_start;   
  for (int i = 0 ; i < epaisseur ; i ++)
  {
    // y0 (mathématique) incrémenté de 2 à chaque itération.
    y0 = nb_lig_img - 40 - (2*i);
    // y0 (mathématique) incrémenté de 1 à chaque itération.
    y1 = y0 - 60 - i ;
    ++x1;
    algo_reveilles(x0 , y0 , x1 , y1 , ImgOut2d , nb_lig_img , nb_col_img , 80 + i*(20));  
  }
  

  conv2Dvers1D( ImgOut , ImgOut2d , nb_lig_img, nb_col_img);
  ecrire_image_pgm(cNomImgEcrite, ImgOut , nb_lig_img , nb_col_img);

  

  
  return 1;
}
