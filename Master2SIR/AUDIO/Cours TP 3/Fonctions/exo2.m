addpath('../../Cours TP 2/Matlab/')
addpath('../Données')
clc;
clear all;
close all;

[hrir , fe_hrir] = hrir_loader(30 , 0 , '1002');
Te = ones(size(hrir , 1)) * (1/fe_hrir);
plot(hrir(: , 1 ) ,Te);
