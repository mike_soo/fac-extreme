clear all; 
clc ;
close all;
load 'releve_vit_cste_axe2.mat'
who

% Question 10

figure();
plot(t,qp2)
xlabel('temps')
ylabel('vitesses sans régimes transitoires [rad/s]')
title('Vitesses mesurées non filtrées au cours du temps');

figure('name' , 'q2, i2, q2, ifil2 , qp2 , ifil2');
plot(q2, i2, q2, ifil2 , qp2 , ifil2)
legend('courantEnreg(positions) : axe2 [A]','courantFiltre(positions) : axe2 [A] ','courantFitlre(vitesse) : axe2 [A]');
title('Résultats axe 2: vitesses , positions , courant enregistré/filtré ');


figure('name' , 'q2 , qp2, ifil2');
plot3(q2, qp2 , ifil2)
xlabel('positions ang axe 2 [rad]')
ylabel('vitesses ang axe 2 [rad/s]')
zlabel('courant filtré axe 2 [A]')
title('Mouvement, positions et courant consommé ')
% Nous remarquons que la vitesse reste constante l'axe tourne dans un sens données. Ceci nous permet de constater que le courant varie par rapport aux positions pertinentes suivantes.
% Pour une vitesse dans le sens horaire:
% On constate l'intensité maximale lorsque $\theta_2 = $

% Nous commençons par construire la matrice Y
% Il s'agit d'une boucle for parcourant tous les éléments de chaque 
% vecteur correspondant respectivement aux positions et vitesses 
% angulaires données. On applique ainsi les opérations nécessaires à chaque 
% colonne afin d'obtenir la matrice Y donnée à l'équation (12)

%% Construction de la matrice Y.
% for(i=1:length(i2)) , %% length(i2) = nombre d'echantillons.
%     Y(i,1:4) = [ cos(q2(i)) sign(qp2(i)) qp2(i) 1 ];
%     u(i,1) = kc2*N2*i2(i);
% end

% Nous calculons par la suite les paramètres alpha2 a2 b2 c2 en appliquant
% l'équation (14)
%% Calcul des paramètres
% p=pinv(Y)*u;

% ident_axe2_v_cste.m
% Nous pouvons ainsi visualiser les valeurs représentées par le 
% vecteur colonne suivant en fonction des données non filtrées. 
%| alpha2 =  0.0853261675953714   
%| a2     =  0.0784331905409513   
%| b2     =  0.000332664823074236 
%| c2     = -0.0118706566850871   

% ident_axe2_v_cste_filtre.m
% On répète le procédé en utilisant les valeurs de courant et de vitesse 
% de rotation filtrées
%|alpha2  =  0.084924593041250
%|a2      =  0.070495234703877
%|b2      =  0.000958000578040
%|c2      = -0.012077896410435

% ident_axe1q14.m
% Il suffit donc de remplacer les constantes données Kc et N  pour obtenir le vecteur
%|alpha1  =  0.382160668685625
%|a1      =  0.317228556167459
%|b1      =  0.00431100260117954
%|c1      =  -0.0543505338469585

% Identification des frottements et de la gravité sur l'axe 2
