Analyse de cours de 05/10/2018 page 1-6
============
Energie mécanique
--
[Em](https://fr.wikipedia.org/wiki/%C3%89nergie_m%C3%A9canique)

[Force conservative](https://fr.wikipedia.org/wiki/Force_conservative)

Energie cinétique
--
[Ec](http://www.japprends-lenergie.fr/ressources/wiki-energie#ressource/sources-d-energie/definition-et-principes/formes-d-energie/energie-de-position-cinetique-et-mecanique)

[Ec wiki](https://fr.wikipedia.org/wiki/%C3%89nergie_cin%C3%A9tique)

Masse ressort
-------------
Comme translations horizontales, le poids de la masse est annulé par celle de la reaction support->masse

Des explications détaillés de formules [physiques](http://physique.chimie.pagesperso-orange.fr/TS_Physique/Physique_18_TRAVAIL_D_UNE_FORCE.htm#3)
du système masse ressort.

Analyse notes de cours de 25/10/2018 page 1-7
============
[Torseur Cinetique](https://fr.wikipedia.org/wiki/Torseur_cin%C3%A9tique)
