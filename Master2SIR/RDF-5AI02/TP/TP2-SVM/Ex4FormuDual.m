clear all;
close all;
clc;

X = [0 0 
	 2 2
	 3 0 
	 2 0] ;

Y =[-1
	-1
	1
	1];

w= [1.2
	-3.2
	];
b=-0.5; 


% svm = SVM(X , Y );

% figure('name' , 'separateur dual');
% hold on;
% svm.calcSeparatorPlan('dual');
% svm.showSeparatorPlan();
% svm.showPoints();
% hold off;

%svm.listenForNewPoints('dual');

X3d=[0 0 1; 
	 2 2 3;
	 3 0 4;
	 2 0 1;
	 ];

figure('name' , 'separateur 3D');
hold on;
svm3d = SVM(X3d , Y);
svm3d.calcSeparatorPlan3D('dual');
svm3d.showSeparatorPlan3D();
svm3d.showPoints();
hold off;