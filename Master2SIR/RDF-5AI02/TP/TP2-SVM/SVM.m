classdef SVM < handle
   properties
   M,N
   X,Y
   w
   wunitaire
   b
   yhyperplan
   sol
   hypx;
   distMin;
   end
   methods
      function obj = SVM(varargin)
         obj.X = varargin{1};
         obj.Y = varargin{2};
         if nargin == 4
            obj.w = varargin{3};
            obj.b = varargin{4};   
         end
         obj.N = size(obj.X , 1);
         obj.M = size(obj.X , 2);


         obj.hypx = [-5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9];
         obj.yhyperplan = zeros(size(obj.hypx , 1));
         obj.sol  = 0;
      end
      
         
      function  calcSeparatorPlan(obj , type)
         if strcmp (type , 'primal')
            obj.primalQuad();
         end
         if strcmp (type , 'dual')
            obj.dualQuad();
         end
         syms y
         for i = 1 : length( obj.hypx)
            sol= solve((obj.w(1)*obj.hypx(i)) +(obj.w(2)*y)+ obj.b == 0, y);
            obj.yhyperplan(i) = sol;
            
         end
         
         

         
         obj.distMin = 1 / norm(obj.w);
         
         obj.wunitaire = obj.w / norm (obj.w);
      end

      function  calcSeparatorPlan3D(obj , type)
         if strcmp (type , 'primal')
            obj.primalQuad();
         end
         if strcmp (type , 'dual')
            obj.dualQuad();
         end
         obj.yhyperplan =[];
         'calculing points of 3D separator plan '
         for i = 1 : length( obj.hypx)
            for j = 1 : length(obj.hypx)
               sol= -((obj.w(1)*i) +(obj.w(2) *j) + obj.b) / obj.w(3);
               obj.yhyperplan =[obj.yhyperplan sol];
            end
         end
         
         

         
         obj.distMin = 1 / norm(obj.w);
         
         obj.wunitaire = obj.w / norm (obj.w);
      end


      function showSeparatorPlan(obj)
         plot(obj.hypx(1 , :) , obj.yhyperplan(1 , :));
         
      end

      function showSeparatorPlan3D(obj)
         k= 1;
         'showing hyperplan points'
         for i = 1 : length(obj.hypx)
            for j = 1: length(obj.hypx)
               % [num2str(i) ' ' num2str(j) ' ' num2str(obj.yhyperplan(k))]
               scatter3(i , j, obj.yhyperplan(k), 15 , 'b'); 
               k = k + 1;
            end
         end
      end

      function addPoint(obj, x)
         obj.X = [obj.X ; x];
         obj.N = obj.N + 1;
         
         if size(obj.w , 1) > 0
            
            if (x * obj.w) + obj.b > 0 
               obj.Y  = [obj.Y;  
                         1];
            else
               obj.Y  = [obj.Y; 
                         -1];
            end
         end 
      end
      
      function showPoints(obj)
         
         Xred=[] ; Xgreen=[];
         for i = 1 : obj.N
            if obj.Y(i) <= 0
               Xred = [Xred; obj.X(i,:)];
            else
               Xgreen = [Xgreen; obj.X(i,:)];
            end
         end
         
         if(obj.M == 2)
            scatter(Xred(: , 1) , Xred(: , 2), 15 , 'r'); 
            scatter(Xgreen(: , 1) , Xgreen(: , 2), 15 , 'g'); 
         
         elseif(obj.M == 3)
            scatter3(Xred(: , 1) , Xred(: , 2), Xred(: , 3) , 15 , 'r'); 
            scatter3(Xgreen(: , 1) , Xgreen(: , 2), Xgreen(: , 3), 15 , 'g'); 

         end

      end
      
      function primalQuad(obj)
         %Optimistaion quadratique

         
         
         C( 1 : obj.N , 1) = 1;

         uns(1:size(obj.X ,1) , 1) = 1;

         A  =  [obj.X uns];
         

         %contient xn1* w1 xn2 *w2 .. xnm*wn + b ou n app { 1 , ... , n} et m correspond
         % a la dimension des points (M);



         C = ones(size( obj.X , 1 ) , 1);

         zer0s(1 , 1 : obj.M + 1) = 0;

         Q = [ 
               eye(obj.M)                  zeros(obj.M  , 1);
               zer0s
            ];

         P = zeros(obj.M + 1,1);


         %multiplication de chaque coordonnées plus cst * Yi pour satisfaire Yn(wn*Xn +b)
         for i = 1 : size( obj.X , 1)
            for j = 1 : obj.M + 1
               A(i,j) = A(i,j) * obj.Y(i);
            end
         end 

         C = - C;
         A = - A;
         %minimizes 1/2*x'*Q*x + p'*x subject to the restrictions A*x ≤ b. The input A is a matrix of doubles, and b is a vector of doubles.
         %original for debug purpose 
         %  x = quadprog(H,f,A,b)
         u = quadprog(Q,P,A,C);
         obj.w = u([1 : obj.M]);
         obj.b = u(obj.M + 1);
      end  

      %!
      %! @brief      start a listening procedure that waits for the user to
      %!             click to the displayed figure so new coordinates can be
      %!             added
      %!
      %! @param      obj   The object
      %! @param      type  The type of search algorithm {'primal','dual'}
      %!
      %! @return     { description_of_the_return_value }
      %!
      function listenForNewPoints(obj , type)
         
         while 1
            [xn] = ginput(1)
            
            obj.addPoint(xn);
            %Si la distance du nouveau point perpendiculaire au plan.
            
            
            xhp = [0 obj.yhyperplan(1)];
            

            if abs( ( xn - xhp ) * obj.wunitaire) < obj.distMin
               if(strcmp(type , 'primal'))
                  obj.calcSeparatorPlan('primal');
               elseif(strcmp(type , 'dual'))
                  obj.calcSeparatorPlan('dual');
               end
            end
            clf
            hold on;
            obj.showSeparatorPlan();
            obj.showPoints();
            hold off;

         end
      end

      function dualQuad(obj)
         Q = (obj.Y * obj.Y');
         for i=1:size(Q , 1)
            for j = 1:size(Q ,2)
               
               Q(i , j)  = Q(i , j) * (obj.X(i,:) * obj.X(j,:)'); 

            end
         end

         Ad = [
             obj.Y' ;
            -obj.Y' ;
            eye(obj.N , obj.N);
         ];

         P = -ones(obj.N , 1)';
         C = zeros(obj.N + 2 , 1);
         % minimizes 1/2*x'*Q*x + p'*x subject to the restrictions A*x ≤ b. The
         % input A is a matrix of doubles, and b is a vector of doubles.
         % original for debug purpose x = quadprog(H,f,A,b)
         a = quadprog(Q,P,-Ad,-C);
         a
         wetoile = zeros(obj.M , 1);
         obj.M
         wetoile
         
         for i = 1 : obj.N
            wetoile = wetoile + ((obj.Y(i) * a(i)) * obj.X(i,:)');
         end
         obj.w = wetoile;
         
         
         
         indexOfxs=1;
         % Looking for the first support vector

         for indexOfxs=1:length(a)
            if a(indexOfxs) > 0.1
               break
            end
         end
         indexOfxs
         
         
         betoile = (1 / obj.Y(indexOfxs)) - (wetoile' * obj.X(indexOfxs , :)');
         obj.b = betoile;
         

         
      end
   end
      
end
