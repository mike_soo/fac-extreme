#!/usr/bin/python3
import math
import os

  # on linux / os x
class MLP:
    def __init__(self , basetrain , labeltrain , basetest , labeltest):
        # ones = np.full((1 , np.shape(basetrain)[1]) , 1)    
        
        ones = np.ones((1 , np.shape(basetrain)[1]), dtype=np.int32 )
        self.basetrain  = np.concatenate((ones , basetrain) , axis=0)

        print("__init__:np.shape(basetest)")
        print(np.shape(basetest))
        print("x1")
        print(basetest)
        ones = np.ones((1 , np.shape(basetest)[1]), dtype=np.int32 )
        self.basetest   = np.concatenate((ones , basetest ) , axis=0) 
        # print("X:")
        # print(self.basetrain)

        self.basetrain  = np.matrix(self.basetrain)
        self.basetest   = np.matrix(self.basetest)

        self.labeltrain = labeltrain
        self.labeltest  = labeltest 
        self.imgsize    = np.shape(basetrain)[0]
        
        

    def getLabelOf(self , type ,i):
        if type == "train":
            return self.labeltrain[i]
        elif type== "test":
            return self.labeltest[i]
        else:
            return -1

    # permettant de générer une matrice de poids aléatoires
    # (distribution uniforme) dont les valeurs sont comprises dans l’intervalle [−1, +1] et de dimension
    
    def mlp1def(self , nbligs , nbcols):
        import random
        self.W=np.random.uniform(-1, 1, nbligs * (nbcols +1)).reshape([nbligs, nbcols + 1])
        self.m = nbligs
        self.nbOfClasses = nbligs
    def sigmox(self , x):
        # print("sigmox: x= "+ str(x))
        return  (1 - math.exp(-2*x)) / (1 + math.exp(-2*x))

    def sigmoV(self , v):
        # print("sigmoV: [self.sigmox(el) for el in v.transpose() ]=" )
        return [self.sigmox(el) for el in v.transpose() ]

    def dsigmox(self , x):        
        return  1 - math.pow(self.sigmox(x) , 2)

    def dsigmoV(self , v):
        
        return [self.dsigmox(el) for el in v.transpose() ].tranpose()


    def mlp1run(self , X):
        self.nbOfImgs   = np.shape(X)[1]
        # print("mlp1run self.W: ")
        # print(self.W)
        # print("mlp1run X:")
        # print(X)
        self.Y = self.W * X
        # print("ml1run: self.Y.transpose()=" )
        # print(self.Y.transpose())
        
        self.Y = np.matrix([self.sigmoV(colEl) for colEl in self.Y.transpose()]).transpose()
        # print("ml1run: self.Y=" )
        # print(self.Y)
        # print("end sigmoverification \n\n")
    
    def mlpclass(self , i):
        # print("mlpclass : =i" + str(i))
        return np.argmax(self.Y[:,i])

    def mlpestimateClasses(self):
        # print("mlpestimatedClasses: estimatedClasses:")        
        self.estimatedClasses = [self.mlpclass(i) for i in range(0 , self.nbOfImgs ) ]
        # print(self.estimatedClasses)

    def setScore(self , label):
        self.score = np.sum([ 1 for i,l in enumerate(label) if l == self.estimatedClasses[i] ])
        # print("score: self.score="    + str(self.score))
        print("setScore: estimatedClasses=")
        print(self.estimatedClasses)

        print("setScore: real classes=")
        print(self.C)

        self.succRate = self.score / len(label)
        print("score: self.succRate=" + str(self.succRate))

        print("self.Y")
        print(self.Y)

        print("self.Ydes")
        print(self.Ydes)
    def label2target(self , C ):
        # print("label2target: C=")
        # print(C)
        self.C = C
        self.Ydes = -1 * np.ones((self.nbOfClasses , len(C)) , dtype = np.int32)
        for i in range(0,len(C)):
            print("i=" + str(i))
            c = C[i]
            self.Ydes[c,i] =  1
        # print("label2target: Ydes")
        # print(self.Ydes)
    
    def error(self , i , j):
        return self.Y[i , j] - self.Ydes[i , j]  

    def setQ(self):
        # print("error: np.power(self.Y - self.Ydes)")
        # print(np.power(self.Y - self.Ydes , 2))
        self.Q=[1/2 *np.sum(np.power(self.Y[:,i] - self.Ydes[:,i] , 2)) for i in range(0 , self.nbOfImgs)]
        print("self.Y[:,i] - self.Ydes[:,i]")
        
        for i in range(0 , self.nbOfImgs):
            print(self.Y[:,i] - self.Ydes[:,i])
        
        print("self.Q=" , end='' , flush = True)
        print(self.Q)

    
    def epsilonik(self , i , k):
        return self.Y[i , k] - self.Ydes[i , k]
        
    # X : base d'aprentissage où test
    # j : column index of a given element (coresponding to the index of an image column)
    # i : line index of a give row element (coresponding for exemple the i-th lvl of the vertical cels layers  )
    def deltaik(self , X , i , k):
        return self.epsilonik(i , k) * self.dsigmox( self.W[i , :] * X[:,k]  )

    def calcdQdw(self , X):
        dQdw= np.zeros((self.m , self.imgsize + 1) , dtype=np.float64)
        for i in range(0 , self.m):
            for j in range(0 , self.imgsize + 1):
                for k in range(0 , self.nbOfImgs):
                    dQdw[i,j] = dQdw[i,j] + (self.deltaik(X , i , k) * X[j , k])
        return dQdw

    def apprentissage(self , lambdavar , X):
        # print("apprentissage: self.W" )
        # print(self.W)
        # print("self.calcdQdw(X):")
        # print(self.calcdQdw(X))

        self.W = self.W - (lambdavar * self.calcdQdw(X))
if __name__ == '__main__':
    # chargement des bases d'apprentissage et de test:
    import numpy as np
    basetrain  = np.load('basetrain.npy')
    labeltrain = np.load('labeltrain.npy')
    basetest   = np.load('basetest.npy')
    labeltest  = np.load('labeltest.npy')
    import matplotlib.pyplot as plt

    # Test de sizes of bases
    # print("len of labeltrain:" + str(len(labeltrain)))
    # print(labeltrain)
    # print("len of labeltest:" + str(len(labeltest)))
    # print(labeltest)
    # print("len of basetrain:" + str(len(basetrain)))
    # print("len of basetrain[1,:]" + str(len(basetrain[1,:])))
    # print("len of basetest:" + str(len(basetest)))
    # print("len of basetest[1,:]" + str(len(basetest[1,:])))

    # affichage:
    # plt.figure(1 , figsize=(3 , 3))
    # plt.imshow( basetest[:,1].reshape(28,28) , cmap=plt.cm.gray_r)
    # plt.show()

    # 3 jeux d’entrees pour un reseau a 5 cellules :
    x = np.array([[-1, -0.5, 0, 0.5, 1], [-1, -1, 0, 0.5, -1], [-1, -2, 0, -0.5, 1]])
    xlabels = [1 , 2 , 3]
    x = np.transpose(x)

    x1 = np.array([[-1, -0.5, 0, 0.5, 1]])
    x1labels = [1]
    x1 = np.transpose(x1)
    # rangement des exemples en colonne:
    # dimensions de la matrice obtenue:
    print("x:")
    print(x)
    print(np.shape(x))

    mlp1 = MLP(x , xlabels , x , xlabels)
    # mlp1 = MLP(x1 , x1labels , x1 , x1labels)
    mlp1.mlp1def(5 , x.shape[0])
    # mlp1.label2target( [1] )
    mlp1.label2target( [1 , 2 , 3] )
    lambdavar = 0.01
    for i in range( 0 , 15000):
        mlp1.mlp1run(mlp1.basetrain)
        mlp1.mlpestimateClasses()
        mlp1.setScore(x1labels)
        mlp1.apprentissage(lambdavar , mlp1.basetrain)
        mlp1.setQ()
        print("\n")
        os.system('clear')
    # mlp1 = MLP(x , labeltrain , x , labeltest)
    # mlp1.mlp1def(3 , x.shape[0])

    # mlp1.mlp1run(X = mlp1.basetrain)
    # mlp1.mlpestimateClasses()
    # mlp1.setScore(xlabels)
    # mlp1.label2target([0,1,2])
    # mlp1.setQ()

    # lambdavar = 0.01
    # for i in range( 0 , 1000):
    #     mlp1.apprentissage(lambdavar , mlp1.basetrain)
    #     mlp1.mlp1run(mlp1.basetrain)
    #     mlp1.setQ()