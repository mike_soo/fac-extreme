#!/usr/bin/python3
import numpy as np


def mlp1def(n, m):
    """ Generate a weight and biais matrix for 1-layer MLP.
        @:param n = must be the number of inputs of the network
        @:param m = number of perceptrons in the 1-layer MLP
        @:returns a [n, m + 1] random uniform matrix of weights (+ bias)"""

    return np.random.uniform(-1, 1, m * (n + 1)).reshape([m, n + 1])


def sigmo(v):
    """ Calculate the sigmoid function of each element in a vector.
        @:param v = argument vector
        @:returns a vector the same size as the argument vector activated by a sigmoid, element-wise"""

    return np.asarray([(1 - np.exp(-2 * el)) / (1 + np.exp(-2 * el)) for el in v], dtype=float)


def sigmop(v):
    """ Calculate the derived sigmoid function of each element in a vector.
        @:param v = argument vector
        @:returns a vector the same size as the argument vector"""

    return np.asarray([1 - np.power(sigmo(el), 2) for el in v])


def label2target(nclass, c):
    """ Convert a label (scalar of vector) into a target vector (or matrix) with desired values (-1 or 1).
        @:param nclass = number of classes in the classification problem
        @:param c = class (assuming class are designed by a integer value from 0 to nclass - 1)
        @:returns a vector or matrix of targets"""

    mat = -1. * np.ones([nclass, c.size], dtype=float)
    for i in range(c.size):
        mat[c[i], i] = 1
    return mat


def mlperror(y, target):
    """ Calculate the error as the difference between the output - target.
        @:param y = (vector or matrix) output of the MLP
        @:param target = (vector or matrix) desired output. y and target must be of the same size.
        @:returns a vector or matrix of errors, same size as y and target """
    
    return y - target


def sqrerror(error):
    """ Calculate the square error.
        @:param error = (vector or matrix) error
        @:returns square error (scalar) """
    
    return 0.5 * np.sum(np.square(error))


def mlp1run(x, w):
    """ Run a 1-layer MLP
        @:param x = input (vector or matrix). Size n * k, n = number of input in one example, k = number of examples
        @:param w = matrix of weights and bias. Size m * (n + 1), m = number of perceptrons
        @:returns output value of the perceptrons. Size m * k """

    V = w.dot(np.concatenate((np.ones([1, np.size(x, 1)]), x), axis=0))
    return np.asarray([sigmo(el) for el in V], dtype=float)


def mlpclass(y):
    """ Get the output class.
        @:param y = output of the MLP (vector or matrix)
        @:returns output class or label (scalar or vector) """
    
    return np.argmax(y, axis=0)


def score(label, labelD):
    """ Calculate the score and rate of the MLP.
        @:param label = output class of the MLP (scalar or vector)
        @:param labelD = desired label. label and labelD must be of the same size.
        @:returns score = number of successful classification
        @:returns rate = rate of successful classification """
    
    score = np.sum(label == labelD)
    rate = score / label.size
    return score, rate


def mlp1train(x, target, w, lr, it):
    """ Train a 1-layer MLP.
        @:param x = input (vector [n,1] or matrix [n,k])
        @:param target = desired output (vector [m,1] or matrix [m,k])
        @:param w = matrix of weights and biais (matrix [m,n+1])
        @:param lr = learning rate (scalar)
        @:param it = number of iteration (scalar)
        @:returns L = quadratic error after the training (scalar)
        @:returns new weights and bias matrix after the training (matrix [m,n+1]) """

    if it is not 0:
        it = it - 1
        exit = mlpclass(mlp1run(x, w))
        error = mlperror(exit, target)
        print("error")
        print(error)
        delta = error.multiply(sigmop(w.dot(x)))
        w = w - lr * delta.multiply(x)
        return mlp1train(x, target, w, lr, it)

    else:
        exit = mlpclass(mlp1run(x, w))
        error = mlperror(exit, target)
        L = sqrerror(error)
        return w, L

if __name__ == '__main__':
    # 3 jeux d’entrees pour un reseau a 5 cellules :
    x = np.array([[-1, -0.5, 0, 0.5, 1], [-1, -1, 0, 0.5, -1], [-1, -2, 0, -0.5, 1]])
    xlabels = [1 , 2 , 3]
    # rangement des exemples en colonne:
    x = np.transpose(x)
    # dimensions de la matrice obtenue:
    print("X:")
    print(x)
    w =  mlp1def(5, 3)
    lr = 0.001
    it = 1000
    mlp1train(x, xlabels, w, lr, it)