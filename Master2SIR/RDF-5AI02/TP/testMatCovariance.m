clear all;
close all;
N= 1000;

sigma =2;
mu=4;
X=randn(N , 2);

X =X 
coVarX = zeros(2 , 2);

for i = 1 : 50
    
    coVarX= coVarX + (X(i,:) * X(i,:)');    
end
coVarX = coVarX / N

figure(1)
hold on

plot(X(: , 1 ) , X(: , 2) , 'o');

[vecsPropres , valsPropre , W ] = eig (coVarX)
vecP1 = vecsPropres(: , 1)
vecP2 = vecsPropres(: , 2)
quiver(0 , 0 , vecP1(1) , vecP1(2)); 
quiver(0 , 0 , vecP2(1) , vecP2(2)); 
pbaspect([1 1 2])