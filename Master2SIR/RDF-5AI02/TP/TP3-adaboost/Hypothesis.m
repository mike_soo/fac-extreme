classdef Hypothesis < handle
	properties
		alphat, separatorCoord , leftSign , errt, dim
	end

	methods	
		function obj = Hypothesis(varargin)
			obj.separatorCoord = varargin{1};
			obj.leftSign 	   = varargin{2};
			obj.errt 		   = varargin{3};
			obj.dim 		   = varargin{4};
			obj.alphat =  (1/2) * log((1 - obj.errt) / obj.errt);
		end
		function c = binClassOf(obj , coordOfXi)
			if( coordOfXi < obj.separatorCoord )
				c =   obj.leftSign;
			else
				c = - obj.leftSign;
			end
		end
	end

end