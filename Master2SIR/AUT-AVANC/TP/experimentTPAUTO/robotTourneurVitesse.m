clear all;
clc;
close all;
%Commande d'un robot tourneur

%I - Analyse du système
% 1 - Etablir la représentation d'état du système 
%    (noté G = y / u) 
%F    Les matrices d'état  seront notées A , B , C et D
%     > Utilisation de la fonction "ss"
% 2 - Calculer les poles du système et analyser la stabilit"
%     > fonction pole
% 3 - analyser la commandabilité et l'observabilité
%     > fonction  ctrb et obsv
% 4 - Tracer la reponse indicielle et determiner 
%    le temps de reponse à 5%
%    >fonction step
% II-A  Commande retour d'état avec prefiltre N
% 5 - Trouver le gain de retour d'état K permettant
%     d'obtenir les poles en boucle fermé à [-5 -7]
%     > fonction place
% 6 - Trouver le préfiltre N permettant d'avoir 
%     une erreur statique nulle
% 7 - Etablir la representation d'état du système
%     en boucle fermé noté Gbf. Les matrices détat 
%     seront notées Abf Bbf Cbf Dbf
% II - B Commande par retour d'état avec observateur 
% identité
% 8 - calculer les matrices F et G d'un 
%     observateur identité. Les poles de l'observateur
%     sont -10 et -20 (utiliser fonction place pour calcul
%     l'observateur identité.
% 9 - programmer sous simulink les systèmes en boucles
%     fermés avec le préfiltre N et l'observateur
%     identité 
% 10 - Tracer la reponse indicielle du système 
%     bouclé pour une reference yref =10 ml/s 
tm = 0.47;
Krpm = 60/(2 * pi);
Kr = 1/32;
Km = 253.05;
fe =100;
A = [-1/tm];
B = [ Km * Krpm* Kr /tm] ;
C = [1];
D = 0;

%I - Analyse du système
% 1 - Etablir la représentation d'état du système 
%    (noté G = y / u) 
%    Les matrices d'état  seront notées A , B , C et D
%     > Utilisation de la fonction "ss"
yref= 4.7123;

sys = ss(A , B , C , D)


% 2 - Calculer les poles du système et analyser la stabilit"
%     > fonction pole
pole(sys)


% 3 - analyser la commandabilité et l'observabilité
%     > fonction  ctrb et obsv
ctrb(sys)
obsv(sys)

% 4 - Tracer la reponse indicielle et determiner 
%    le temps de reponse à 5%
%    >fonction step

step(sys)
stepinfo(sys , 'SettlingTimeThreshold',0.05)

% II-A  Commande retour d'état avec prefiltre N
% 5 - Trouver le gain de retour d'état K permettant
%     d'obtenir les poles en boucle fermé à [-5 -7]
%     > fonction place
P = [-5];
% calcul de gain de retour d'état K
K = place (A , B , P)
 
% 6 - Trouver le préfiltre N permettant d'avoir 
%     une erreur statique nulle
% Calcul de prefiltre
%'A - BK'
N = -1 / (C * inv(A - (B*K)) * B)



% 7 - Etablir la representation d'état du système
%     en boucle fermé noté Gbf. Les matrices détat 
%     seront notées Abf Bbf Cbf Dbf
Abf = A - (B*K);
Bbf = B * N ;
Cbf = C;
Dbf = D;
Pbf = [-20];
sysbf = ss(Abf , Bbf , Cbf , Dbf)


% II - B Commande par retour d'état avec observateur 
% identité
% 8 - calculer les matrices F et G d'un 
%     observateur identité. Les poles de l'observateur
%     sont à determiner (utiliser fonction place pour calcul
%     l'observateur identité.
Ppos_obs_des = [-40];
GT = place( A' , C' , Ppos_obs_des) 
G = GT'
F = A - (G * C)

%II - C Commande par retour d'état avec intégrateur

Ai = [ A zeros(size(A,2) , 1);
       C zeros(1 , 1)];
Bi = [ B;
       zeros(1,1) ];
poleInteg = [-5; -40] ;
Ki = place( Ai , Bi , poleInteg);
Kinteg = Ki(:,1:end-1);
Ki_3 = Ki(1,end);